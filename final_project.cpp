#include "final_project.h"
#include "ui_final_project.h"
#include <iostream>
#include <Qstring>

Final_Project::Final_Project(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Final_Project)
{
    ui->setupUi(this);
}

Final_Project::~Final_Project()
{
    delete ui;
}

void Final_Project::on_Number_of_players_valueChanged(int arg1)
{
    if (ui->Number_of_players->value() == 0)
    {
        ui->label_6->setDisabled(true);

        ui->Name_entry_continue->setDisabled(true);

        ui->Player1_name->setDisabled(true);
        ui->plainTextEdit->setDisabled(true);
        ui->label_7->setDisabled(true);
        ui->Player1_money->setDisabled(true);

        ui->Player2_name->setDisabled(true);
        ui->plainTextEdit_2->setDisabled(true);
        ui->label_9->setDisabled(true);
        ui->Player2_money->setDisabled(true);

        ui->Player3_name->setDisabled(true);
        ui->plainTextEdit_3->setDisabled(true);
        ui->label_8->setDisabled(true);
        ui->Player3_money->setDisabled(true);

        ui->Player4_name->setDisabled(true);
        ui->plainTextEdit_5->setDisabled(true);
        ui->label_2->setDisabled(true);
        ui->Player4_money->setDisabled(true);

        ui->Player5_name->setDisabled(true);
        ui->plainTextEdit_4->setDisabled(true);
        ui->label->setDisabled(true);
        ui->Player5_money->setDisabled(true);
    }
    if (ui->Number_of_players->value() == 1)
    {
        ui->label_6->setDisabled(false);

        ui->Name_entry_continue->setDisabled(false);

        ui->Player1_name->setDisabled(false);
        ui->plainTextEdit->setDisabled(false);
        ui->label_7->setDisabled(false);
        ui->Player1_money->setDisabled(false);

        ui->Player2_name->setDisabled(true);
        ui->plainTextEdit_2->setDisabled(true);
        ui->label_9->setDisabled(true);
        ui->Player2_money->setDisabled(true);

        ui->Player3_name->setDisabled(true);
        ui->plainTextEdit_3->setDisabled(true);
        ui->label_8->setDisabled(true);
        ui->Player3_money->setDisabled(true);

        ui->Player4_name->setDisabled(true);
        ui->plainTextEdit_5->setDisabled(true);
        ui->label_2->setDisabled(true);
        ui->Player4_money->setDisabled(true);

        ui->Player5_name->setDisabled(true);
        ui->plainTextEdit_4->setDisabled(true);
        ui->label->setDisabled(true);
        ui->Player5_money->setDisabled(true);
    }
    if(ui->Number_of_players->value() == 2)
    {
        ui->label_6->setDisabled(false);

        ui->Name_entry_continue->setDisabled(false);

        ui->Player1_name->setDisabled(false);
        ui->plainTextEdit->setDisabled(false);
        ui->label_7->setDisabled(false);
        ui->Player1_money->setDisabled(false);

        ui->Player2_name->setDisabled(false);
        ui->plainTextEdit_2->setDisabled(false);
        ui->label_9->setDisabled(false);
        ui->Player2_money->setDisabled(false);

        ui->Player3_name->setDisabled(true);
        ui->plainTextEdit_3->setDisabled(true);
        ui->label_8->setDisabled(true);
        ui->Player3_money->setDisabled(true);

        ui->Player4_name->setDisabled(true);
        ui->plainTextEdit_5->setDisabled(true);
        ui->label_2->setDisabled(true);
        ui->Player4_money->setDisabled(true);

        ui->Player5_name->setDisabled(true);
        ui->plainTextEdit_4->setDisabled(true);
        ui->label->setDisabled(true);
        ui->Player5_money->setDisabled(true);
    }
    if(ui->Number_of_players->value() == 3)
    {
        ui->label_6->setDisabled(false);

        ui->Name_entry_continue->setDisabled(false);

        ui->Player1_name->setDisabled(false);
        ui->plainTextEdit->setDisabled(false);
        ui->label_7->setDisabled(false);
        ui->Player1_money->setDisabled(false);

        ui->Player2_name->setDisabled(false);
        ui->plainTextEdit_2->setDisabled(false);
        ui->label_9->setDisabled(false);
        ui->Player2_money->setDisabled(false);

        ui->Player3_name->setDisabled(false);
        ui->plainTextEdit_3->setDisabled(false);
        ui->label_8->setDisabled(false);
        ui->Player3_money->setDisabled(false);

        ui->Player4_name->setDisabled(true);
        ui->plainTextEdit_5->setDisabled(true);
        ui->label_2->setDisabled(true);
        ui->Player4_money->setDisabled(true);

        ui->Player5_name->setDisabled(true);
        ui->plainTextEdit_4->setDisabled(true);
        ui->label->setDisabled(true);
        ui->Player5_money->setDisabled(true);
    }
    if(ui->Number_of_players->value() == 4)
    {
        ui->label_6->setDisabled(false);

        ui->Name_entry_continue->setDisabled(false);

        ui->Player1_name->setDisabled(false);
        ui->plainTextEdit->setDisabled(false);
        ui->label_7->setDisabled(false);
        ui->Player1_money->setDisabled(false);

        ui->Player2_name->setDisabled(false);
        ui->plainTextEdit_2->setDisabled(false);
        ui->label_9->setDisabled(false);
        ui->Player2_money->setDisabled(false);

        ui->Player3_name->setDisabled(false);
        ui->plainTextEdit_3->setDisabled(false);
        ui->label_8->setDisabled(false);
        ui->Player3_money->setDisabled(false);

        ui->Player4_name->setDisabled(false);
        ui->plainTextEdit_5->setDisabled(false);
        ui->label_2->setDisabled(false);
        ui->Player4_money->setDisabled(false);

        ui->Player5_name->setDisabled(true);
        ui->plainTextEdit_4->setDisabled(true);
        ui->label->setDisabled(true);
        ui->Player5_money->setDisabled(true);
    }
    if (ui->Number_of_players->value() == 5)
    {
        ui->label_6->setDisabled(false);

        ui->Name_entry_continue->setDisabled(false);

        ui->Player1_name->setDisabled(false);
        ui->plainTextEdit->setDisabled(false);
        ui->label_7->setDisabled(false);
        ui->Player1_money->setDisabled(false);

        ui->Player2_name->setDisabled(false);
        ui->plainTextEdit_2->setDisabled(false);
        ui->label_9->setDisabled(false);
        ui->Player2_money->setDisabled(false);

        ui->Player3_name->setDisabled(false);
        ui->plainTextEdit_3->setDisabled(false);
        ui->label_8->setDisabled(false);
        ui->Player3_money->setDisabled(false);

        ui->Player4_name->setDisabled(false);
        ui->plainTextEdit_5->setDisabled(false);
        ui->label_2->setDisabled(false);
        ui->Player4_money->setDisabled(false);

        ui->Player5_name->setDisabled(false);
        ui->plainTextEdit_4->setDisabled(false);
        ui->label->setDisabled(false);
        ui->Player5_money->setDisabled(false);
    }
}

void Final_Project::on_Name_entry_continue_clicked()
{
    if (ui->Number_of_players->value() == 1)
        players.push_back(Player(ui->plainTextEdit->toPlainText().toStdString(), ui->Player1_money->value()));
    if (ui->Number_of_players->value() == 2)
    {
        players.push_back(Player(ui->plainTextEdit->toPlainText().toStdString(), ui->Player1_money->value()));
        players.push_back(Player(ui->plainTextEdit_2->toPlainText().toStdString(), ui->Player2_money->value()));
    }
    if (ui->Number_of_players->value() == 3)
    {
        players.push_back(Player(ui->plainTextEdit->toPlainText().toStdString(), ui->Player1_money->value()));
        players.push_back(Player(ui->plainTextEdit_2->toPlainText().toStdString(), ui->Player2_money->value()));
        players.push_back(Player(ui->plainTextEdit_3->toPlainText().toStdString(), ui->Player3_money->value()));
    }
    if (ui->Number_of_players->value() == 4)
    {
        players.push_back(Player(ui->plainTextEdit->toPlainText().toStdString(), ui->Player1_money->value()));
        players.push_back(Player(ui->plainTextEdit_2->toPlainText().toStdString(), ui->Player2_money->value()));
        players.push_back(Player(ui->plainTextEdit_3->toPlainText().toStdString(), ui->Player3_money->value()));
        players.push_back(Player(ui->plainTextEdit_5->toPlainText().toStdString(), ui->Player4_money->value()));
    }
    if (ui->Number_of_players->value() == 5)
    {
        players.push_back(Player(ui->plainTextEdit->toPlainText().toStdString(), ui->Player1_money->value()));
        players.push_back(Player(ui->plainTextEdit_2->toPlainText().toStdString(), ui->Player2_money->value()));
        players.push_back(Player(ui->plainTextEdit_3->toPlainText().toStdString(), ui->Player3_money->value()));
        players.push_back(Player(ui->plainTextEdit_5->toPlainText().toStdString(), ui->Player4_money->value()));
        players.push_back(Player(ui->plainTextEdit_4->toPlainText().toStdString(), ui->Player5_money->value()));
    }

    ui->stackedWidget->setCurrentIndex(1);
    if (players.size() < 2)
        ui->Texas_hold_em_button->setDisabled(true);
}

void Final_Project::on_pushButton_4_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void Final_Project::on_Blackjack_button_clicked()
{
    ui->Blackjack_button->setEnabled(false);
    ui->stackedWidget->setCurrentIndex(2);
    if (players.size() >= 1)
    {
        ui->Player1_bet->setEnabled(true);
        ui->label_18->setEnabled(true);
        ui->Player1_bet_name->setText(QString::fromStdString(players[0].get_name() + " ($") + QString::number(players[0].get_money()) + QString::fromStdString(")"));
        ui->Player1_bet->setMaximum(players[0].get_money());
    }
    if (players.size() >= 2)
    {
        ui->Player2_bet->setEnabled(true);
        ui->label_16->setEnabled(true);
        ui->Player2_bet_name->setText(QString::fromStdString(players[1].get_name() + " ($") + QString::number(players[1].get_money()) + QString::fromStdString(")"));
        ui->Player2_bet->setMaximum(players[1].get_money());
    }
    if (players.size() >= 3)
    {
        ui->Player3_bet->setEnabled(true);
        ui->label_17->setEnabled(true);
        ui->Player3_bet_name->setText(QString::fromStdString(players[2].get_name() + " ($") + QString::number(players[2].get_money()) + QString::fromStdString(")"));
        ui->Player3_bet->setMaximum(players[2].get_money());
    }
    if (players.size() >= 4)
    {
        ui->Player4_bet->setEnabled(true);
        ui->label_4->setEnabled(true);
        ui->Player4_bet_name->setText(QString::fromStdString(players[3].get_name() + " ($") + QString::number(players[3].get_money()) + QString::fromStdString(")"));
        ui->Player4_bet->setMaximum(players[3].get_money());
    }
    if (players.size() == 5)
    {
        ui->Player5_bet->setEnabled(true);
        ui->label_5->setEnabled(true);
        ui->Player5_bet_name->setText(QString::fromStdString(players[4].get_name() + " ($") + QString::number(players[4].get_money()) + QString::fromStdString(")"));
        ui->Player5_bet->setMaximum(players[4].get_money());
    }
}

void Final_Project::on_Texas_hold_em_button_clicked()
{
    ui->stackedWidget->setCurrentIndex(5);
    ui->Texas_hold_em_button->setEnabled(false);
    max_bet = players[0].get_money();
    for (int i = 0; i < players.size(); i++)
    {
        if (players[i].get_money() < max_bet)
            max_bet = players[i].get_money();
    }
    ui->Max_bet->setText(QString::number(max_bet));
    ui->Big_blind->setMaximum(max_bet);
    current_bet = max_bet;
}

void Final_Project::on_Craps_button_clicked()
{
    ui->Craps_button->setEnabled(false);
    ui->stackedWidget->setCurrentIndex(11);
    if (players.size() >= 1)
    {
        ui->Player1_craps->setText(QString::fromStdString(players[0].get_name()));
        ui->label_107->setEnabled(true);
        ui->Player1_bet_craps->setEnabled(true);
        ui->Player1_bet_craps->setMaximum(players[0].get_money());
    }
    if (players.size() >= 2)
    {
        ui->Player2_craps->setText(QString::fromStdString(players[1].get_name()));
        ui->label_108->setEnabled(true);
        ui->Player2_bet_craps->setEnabled(true);
        ui->Player2_bet_craps->setMaximum(players[1].get_money());
    }
    if (players.size() >= 3)
    {
        ui->Player3_craps->setText(QString::fromStdString(players[2].get_name()));
        ui->label_109->setEnabled(true);
        ui->Player3_bet_craps->setEnabled(true);
        ui->Player3_bet_craps->setMaximum(players[2].get_money());
    }
    if (players.size() >= 4)
    {
        ui->Player4_craps->setText(QString::fromStdString(players[3].get_name()));
        ui->label_110->setEnabled(true);
        ui->Player4_bet_craps->setEnabled(true);
        ui->Player4_bet_craps->setMaximum(players[3].get_money());
    }
    if (players.size() == 5)
    {
        ui->Player5_craps->setText(QString::fromStdString(players[4].get_name()));
        ui->label_111->setEnabled(true);
        ui->Player5_bet_craps->setEnabled(true);
        ui->Player5_bet_craps->setMaximum(players[4].get_money());
    }
}

void Final_Project::on_Bet_entry_continue_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);

    srand((int)time(0));
    deck.shuffle();

    while (dealer.get_hands()[0]->get_size() < 2)
    {
        for (Player& i : players)
        {
            i.get_hands()[0]->add_card(deck.deal_card());
        }
        dealer.get_hands()[0]->add_card(deck.deal_card());
    }

    ui->Dealers_card->setText(QString::fromStdString(dealer.get_hands()[0]->get_card(0).print()));

    if (players.size() >= 1)
    {
        ui->Player1_cards->setText(QString::fromStdString(players[0].get_name()));
        ui->Player1_hand1->setText(QString::fromStdString(players[0].print_hand(0)));
        ui->Stay1->setEnabled(true);
        ui->Hit1->setEnabled(true);
        if (players[0].get_money() >= 2 * players[0].get_hands()[0]->get_bet())
            ui->Double_down1->setEnabled(true);
        if (players[0].get_money() >= 2 * players[0].get_hands()[0]->get_bet() && players[0].get_hands()[0]->get_card(0).get_face() == players[0].get_hands()[0]->get_card(1).get_face())
            ui->Split1->setEnabled(true);

        players[0].get_hands()[0]->set_bet(ui->Player1_bet->value());
    }
    if (players.size() >= 2)
    {
        ui->Player2_cards->setText(QString::fromStdString(players[1].get_name()));
        ui->Player2_hand1->setText(QString::fromStdString(players[1].print_hand(0)));
        ui->Stay2->setEnabled(true);
        ui->Hit2->setEnabled(true);
        if (players[1].get_money() >= 2 * players[1].get_hands()[0]->get_bet())
            ui->Double_down2->setEnabled(true);
        if (players[1].get_money() >= 2 * players[1].get_hands()[0]->get_bet() && players[1].get_hands()[0]->get_card(0).get_face() == players[1].get_hands()[0]->get_card(1).get_face())
            ui->Split2->setEnabled(true);

        players[1].get_hands()[0]->set_bet(ui->Player2_bet->value());
    }
    if (players.size() >= 3)
    {
        ui->Player3_cards->setText(QString::fromStdString(players[2].get_name()));
        ui->Player3_hand1->setText(QString::fromStdString(players[2].print_hand(0)));
        ui->Stay3->setEnabled(true);
        ui->Hit3->setEnabled(true);
        if (players[2].get_money() >= 2 * players[2].get_hands()[0]->get_bet())
            ui->Double_down3->setEnabled(true);
        if (players[2].get_money() >= 2 * players[2].get_hands()[0]->get_bet() && players[2].get_hands()[0]->get_card(0).get_face() == players[2].get_hands()[0]->get_card(1).get_face())
            ui->Split3->setEnabled(true);

        players[2].get_hands()[0]->set_bet(ui->Player3_bet->value());
    }
    if (players.size() >= 4)
    {
        ui->Player4_cards->setText(QString::fromStdString(players[3].get_name()));
        ui->Player4_hand1->setText(QString::fromStdString(players[3].print_hand(0)));
        ui->Stay4->setEnabled(true);
        ui->Hit4->setEnabled(true);
        if (players[3].get_money() >= 2 * players[3].get_hands()[0]->get_bet())
            ui->Double_down4->setEnabled(true);
        if (players[3].get_money() >= 2 * players[3].get_hands()[0]->get_bet() && players[3].get_hands()[0]->get_card(0).get_face() == players[3].get_hands()[0]->get_card(1).get_face())
            ui->Split4->setEnabled(true);

        players[3].get_hands()[0]->set_bet(ui->Player4_bet->value());
    }
    if (players.size() >= 5)
    {
        ui->Player5_cards->setText(QString::fromStdString(players[4].get_name()));
        ui->Player5_hand1->setText(QString::fromStdString(players[4].print_hand(0)));
        ui->Stay5_1->setEnabled(true);
        ui->Hit5->setEnabled(true);
        if (players[4].get_money() >= 2 * players[4].get_hands()[0]->get_bet())
            ui->Double_down5->setEnabled(true);
        if (players[4].get_money() >= 2 * players[4].get_hands()[0]->get_bet() && players[4].get_hands()[0]->get_card(0).get_face() == players[4].get_hands()[0]->get_card(1).get_face())
            ui->Split5->setEnabled(true);

        players[4].get_hands()[0]->set_bet(ui->Player5_bet->value());
    }
}

void Final_Project::on_Stay1_clicked()
{
    ui->Hit1->setEnabled(false);
    ui->Double_down1->setEnabled(false);
    ui->Split1->setEnabled(false);
    ui->Stay1->setEnabled(false);
}

void Final_Project::on_Hit1_clicked()
{
    players[0].get_hands()[0]->add_card(deck.deal_card());
    ui->Player1_hand1->setText(QString::fromStdString(players[0].print_hand(0)));
    if (players[0].get_hands()[0]->get_sum() >= 21)
    {
        ui->Hit1->setEnabled(false);
        ui->Stay1->setEnabled(false);
    }
    ui->Double_down1->setEnabled(false);
    ui->Split1->setEnabled(false);
}

void Final_Project::on_Double_down1_clicked()
{
    players[0].get_hands()[0]->add_card(deck.deal_card());
    players[0].get_hands()[0]->set_bet(2 * players[0].get_hands()[0]->get_bet());
    ui->Player1_hand1->setText(QString::fromStdString(players[0].print_hand(0)));
    ui->Stay1->setEnabled(false);
    ui->Hit1->setEnabled(false);
    ui->Double_down1->setEnabled(false);
    ui->Split1->setEnabled(false);
}

void Final_Project::on_Split1_clicked()
{
    players[0].add_hand(new Hand(players[0].get_hands()[0]->get_bet()));
    players[0].get_hands()[1]->add_card(players[0].get_hands()[0]->get_card(1));
    players[0].get_hands()[0]->remove_last_card();
    players[0].get_hands()[0]->add_card(deck.deal_card());
    players[0].get_hands()[1]->add_card(deck.deal_card());
    ui->Player1_hand1->setText(QString::fromStdString(players[0].print_hand(0)));
    ui->Player1_hand2->setText(QString::fromStdString(players[0].print_hand(1)));
    ui->Split1->setEnabled(false);
}

void Final_Project::on_Stay1_2_clicked()
{
    ui->Hit1_2->setEnabled(false);
    ui->Double_down1_2->setEnabled(false);
    ui->Stay1_2->setEnabled(false);
}

void Final_Project::on_Hit1_2_clicked()
{
    players[0].get_hands()[1]->add_card(deck.deal_card());
    ui->Player1_hand2->setText(QString::fromStdString(players[0].print_hand(1)));
    if (players[0].get_hands()[1]->get_sum() >= 21)
    {
        ui->Hit1_2->setEnabled(false);
        ui->Stay1_2->setEnabled(false);
    }
    ui->Double_down1_2->setEnabled(false);
}

void Final_Project::on_Double_down1_2_clicked()
{
    players[0].get_hands()[1]->add_card(deck.deal_card());
    players[0].get_hands()[1]->set_bet(2 * players[0].get_hands()[1]->get_bet());
    ui->Player1_hand2->setText(QString::fromStdString(players[0].print_hand(1)));
    ui->Stay1_2->setEnabled(false);
    ui->Hit1_2->setEnabled(false);
    ui->Double_down1_2->setEnabled(false);
}



void Final_Project::on_Stay2_clicked()
{
    ui->Hit2->setEnabled(false);
    ui->Double_down2->setEnabled(false);
    ui->Split2->setEnabled(false);
    ui->Stay2->setEnabled(false);
}

void Final_Project::on_Hit2_clicked()
{
    players[1].get_hands()[0]->add_card(deck.deal_card());
    ui->Player2_hand1->setText(QString::fromStdString(players[1].print_hand(0)));
    if (players[1].get_hands()[0]->get_sum() >= 21)
    {
        ui->Hit2->setEnabled(false);
        ui->Stay2->setEnabled(false);
    }
    ui->Double_down2->setEnabled(false);
    ui->Split2->setEnabled(false);
}

void Final_Project::on_Double_down2_clicked()
{
    players[1].get_hands()[0]->add_card(deck.deal_card());
    players[1].get_hands()[0]->set_bet(2 * players[1].get_hands()[0]->get_bet());
    ui->Player2_hand1->setText(QString::fromStdString(players[1].print_hand(0)));
    ui->Stay2->setEnabled(false);
    ui->Hit2->setEnabled(false);
    ui->Double_down2->setEnabled(false);
    ui->Split2->setEnabled(false);
}

void Final_Project::on_Split2_clicked()
{
    players[1].add_hand(new Hand(players[1].get_hands()[0]->get_bet()));
    players[1].get_hands()[1]->add_card(players[1].get_hands()[0]->get_card(1));
    players[1].get_hands()[0]->remove_last_card();
    players[1].get_hands()[0]->add_card(deck.deal_card());
    players[1].get_hands()[1]->add_card(deck.deal_card());
    ui->Player2_hand1->setText(QString::fromStdString(players[1].print_hand(0)));
    ui->Player2_hand2->setText(QString::fromStdString(players[1].print_hand(1)));
    ui->Split2->setEnabled(false);
}

void Final_Project::on_Stay2_2_clicked()
{
    ui->Hit2_2->setEnabled(false);
    ui->Double_down2_2->setEnabled(false);
    ui->Stay2_2->setEnabled(false);
}

void Final_Project::on_Hit2_2_clicked()
{
    players[1].get_hands()[1]->add_card(deck.deal_card());
    ui->Player2_hand2->setText(QString::fromStdString(players[1].print_hand(1)));
    if (players[1].get_hands()[1]->get_sum() >= 21)
    {
        ui->Hit2_2->setEnabled(false);
        ui->Stay2_2->setEnabled(false);
    }
    ui->Double_down2_2->setEnabled(false);
}

void Final_Project::on_Double_down2_2_clicked()
{
    players[1].get_hands()[1]->add_card(deck.deal_card());
    players[1].get_hands()[1]->set_bet(2 * players[1].get_hands()[1]->get_bet());
    ui->Player2_hand2->setText(QString::fromStdString(players[1].print_hand(1)));
    ui->Stay2_2->setEnabled(false);
    ui->Hit2_2->setEnabled(false);
    ui->Double_down2_2->setEnabled(false);
}

void Final_Project::on_Stay3_clicked()
{
    ui->Hit3->setEnabled(false);
    ui->Double_down3->setEnabled(false);
    ui->Split3->setEnabled(false);
    ui->Stay3->setEnabled(false);
}

void Final_Project::on_Hit3_clicked()
{
    players[2].get_hands()[0]->add_card(deck.deal_card());
    ui->Player3_hand1->setText(QString::fromStdString(players[2].print_hand(0)));
    if (players[2].get_hands()[0]->get_sum() >= 21)
    {
        ui->Hit3->setEnabled(false);
        ui->Stay3->setEnabled(false);
    }
    ui->Double_down3->setEnabled(false);
    ui->Split3->setEnabled(false);
}

void Final_Project::on_Double_down3_clicked()
{
    players[2].get_hands()[0]->add_card(deck.deal_card());
    players[2].get_hands()[0]->set_bet(2 * players[2].get_hands()[0]->get_bet());
    ui->Player3_hand1->setText(QString::fromStdString(players[2].print_hand(0)));
    ui->Stay3->setEnabled(false);
    ui->Hit3->setEnabled(false);
    ui->Double_down3->setEnabled(false);
    ui->Split3->setEnabled(false);
}

void Final_Project::on_Split3_clicked()
{
    players[2].add_hand(new Hand(players[2].get_hands()[0]->get_bet()));
    players[2].get_hands()[1]->add_card(players[2].get_hands()[0]->get_card(1));
    players[2].get_hands()[0]->remove_last_card();
    players[2].get_hands()[0]->add_card(deck.deal_card());
    players[2].get_hands()[1]->add_card(deck.deal_card());
    ui->Player3_hand1->setText(QString::fromStdString(players[2].print_hand(0)));
    ui->Player3_hand2->setText(QString::fromStdString(players[2].print_hand(1)));
    ui->Split3->setEnabled(false);
}

void Final_Project::on_Stay3_2_clicked()
{
    ui->Hit3_2->setEnabled(false);
    ui->Double_down3_2->setEnabled(false);
    ui->Stay3_2->setEnabled(false);
}

void Final_Project::on_Hit3_2_clicked()
{
    players[2].get_hands()[1]->add_card(deck.deal_card());
    ui->Player3_hand2->setText(QString::fromStdString(players[2].print_hand(1)));
    if (players[2].get_hands()[1]->get_sum() >= 21)
    {
        ui->Hit3_2->setEnabled(false);
        ui->Stay3_2->setEnabled(false);
    }
    ui->Double_down3_2->setEnabled(false);
}

void Final_Project::on_Double_down3_2_clicked()
{
    players[2].get_hands()[1]->add_card(deck.deal_card());
    players[2].get_hands()[1]->set_bet(2 * players[2].get_hands()[1]->get_bet());
    ui->Player3_hand2->setText(QString::fromStdString(players[2].print_hand(1)));
    ui->Stay3_2->setEnabled(false);
    ui->Hit3_2->setEnabled(false);
    ui->Double_down3_2->setEnabled(false);
}

void Final_Project::on_Stay4_clicked()
{
    ui->Hit4->setEnabled(false);
    ui->Double_down4->setEnabled(false);
    ui->Split4->setEnabled(false);
    ui->Stay4->setEnabled(false);
}

void Final_Project::on_Hit4_clicked()
{
    players[3].get_hands()[0]->add_card(deck.deal_card());
    ui->Player4_hand1->setText(QString::fromStdString(players[3].print_hand(0)));
    if (players[3].get_hands()[0]->get_sum() >= 21)
    {
        ui->Hit4->setEnabled(false);
        ui->Stay4->setEnabled(false);
    }
    ui->Double_down4->setEnabled(false);
    ui->Split4->setEnabled(false);
}

void Final_Project::on_Double_down4_clicked()
{
    players[3].get_hands()[0]->add_card(deck.deal_card());
    players[3].get_hands()[0]->set_bet(2 * players[3].get_hands()[0]->get_bet());
    ui->Player4_hand1->setText(QString::fromStdString(players[3].print_hand(0)));
    ui->Stay4->setEnabled(false);
    ui->Hit4->setEnabled(false);
    ui->Double_down4->setEnabled(false);
    ui->Split4->setEnabled(false);
}

void Final_Project::on_Split4_clicked()
{
    players[3].add_hand(new Hand(players[3].get_hands()[0]->get_bet()));
    players[3].get_hands()[1]->add_card(players[3].get_hands()[0]->get_card(1));
    players[3].get_hands()[0]->remove_last_card();
    players[3].get_hands()[0]->add_card(deck.deal_card());
    players[3].get_hands()[1]->add_card(deck.deal_card());
    ui->Player4_hand1->setText(QString::fromStdString(players[3].print_hand(0)));
    ui->Player4_hand2->setText(QString::fromStdString(players[3].print_hand(1)));
    ui->Split4->setEnabled(false);
}

void Final_Project::on_Stay4_2_clicked()
{
    ui->Hit4_2->setEnabled(false);
    ui->Double_down4_2->setEnabled(false);
    ui->Stay4_2->setEnabled(false);
}

void Final_Project::on_Hit4_2_clicked()
{
    players[3].get_hands()[1]->add_card(deck.deal_card());
    ui->Player4_hand2->setText(QString::fromStdString(players[3].print_hand(1)));
    if (players[3].get_hands()[1]->get_sum() >= 21)
    {
        ui->Hit4_2->setEnabled(false);
        ui->Stay4_2->setEnabled(false);
    }
    ui->Double_down4_2->setEnabled(false);
}

void Final_Project::on_Double_down4_2_clicked()
{
    players[3].get_hands()[1]->add_card(deck.deal_card());
    players[3].get_hands()[1]->set_bet(2 * players[3].get_hands()[1]->get_bet());
    ui->Player4_hand2->setText(QString::fromStdString(players[3].print_hand(1)));
    ui->Stay4_2->setEnabled(false);
    ui->Hit4_2->setEnabled(false);
    ui->Double_down4_2->setEnabled(false);
}

void Final_Project::on_Stay5_1_clicked()
{
    ui->Hit5->setEnabled(false);
    ui->Double_down5->setEnabled(false);
    ui->Split5->setEnabled(false);
    ui->Stay5_1->setEnabled(false);
}

void Final_Project::on_Hit5_clicked()
{
    players[4].get_hands()[0]->add_card(deck.deal_card());
    ui->Player5_hand1->setText(QString::fromStdString(players[4].print_hand(0)));
    if (players[4].get_hands()[0]->get_sum() >= 21)
    {
        ui->Hit5->setEnabled(false);
        ui->Stay5_1->setEnabled(false);
    }
    ui->Double_down5->setEnabled(false);
    ui->Split5->setEnabled(false);
}

void Final_Project::on_Double_down5_clicked()
{
    players[4].get_hands()[0]->add_card(deck.deal_card());
    players[4].get_hands()[0]->set_bet(2 * players[4].get_hands()[0]->get_bet());
    ui->Player5_hand1->setText(QString::fromStdString(players[4].print_hand(0)));
    ui->Stay5_1->setEnabled(false);
    ui->Hit5->setEnabled(false);
    ui->Double_down5->setEnabled(false);
    ui->Split5->setEnabled(false);
}

void Final_Project::on_Split5_clicked()
{
    players[4].add_hand(new Hand(players[4].get_hands()[0]->get_bet()));
    players[4].get_hands()[1]->add_card(players[4].get_hands()[0]->get_card(1));
    players[4].get_hands()[0]->remove_last_card();
    players[4].get_hands()[0]->add_card(deck.deal_card());
    players[4].get_hands()[1]->add_card(deck.deal_card());
    ui->Player5_hand1->setText(QString::fromStdString(players[4].print_hand(0)));
    ui->Player5_hand2->setText(QString::fromStdString(players[4].print_hand(1)));
    ui->Split5->setEnabled(false);
}

void Final_Project::on_Stay5_2_clicked()
{
    ui->Hit5_2->setEnabled(false);
    ui->Double_down5_2->setEnabled(false);
    ui->Stay5_2->setEnabled(false);
}

void Final_Project::on_Hit5_2_clicked()
{
    players[4].get_hands()[1]->add_card(deck.deal_card());
    ui->Player5_hand2->setText(QString::fromStdString(players[4].print_hand(1)));
    if (players[4].get_hands()[1]->get_sum() >= 21)
    {
        ui->Hit5_2->setEnabled(false);
        ui->Stay5_2->setEnabled(false);
    }
    ui->Double_down5_2->setEnabled(false);
}

void Final_Project::on_Double_down5_2_clicked()
{
    players[4].get_hands()[1]->add_card(deck.deal_card());
    players[4].get_hands()[1]->set_bet(2 * players[4].get_hands()[1]->get_bet());
    ui->Player5_hand2->setText(QString::fromStdString(players[4].print_hand(1)));
    ui->Stay5_2->setEnabled(false);
    ui->Hit5_2->setEnabled(false);
    ui->Double_down5_2->setEnabled(false);
}

void Final_Project::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(4);
    while (dealer.get_hands()[0]->get_sum() < 17)
    {
        dealer.get_hands()[0]->add_card(deck.deal_card());
    }
    ui->Dealers_cards->setText(QString::fromStdString(dealer.print_hand(0)));

    if (players.size() >= 1)
    {
        if (players[0].get_hands()[0]->get_sum() > 21)
        {
            ui->Player1_hand1_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[0].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[0].lost_round(players[0].get_hands()[0]->get_bet());
        }
        else if (players[0].get_hands()[0]->get_sum() < dealer.get_hands()[0]->get_sum() && dealer.get_hands()[0]->get_sum() <= 21)
        {
            ui->Player1_hand1_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[0].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[0].lost_round(players[0].get_hands()[0]->get_bet());
        }
        else if (players[0].get_hands()[0]->get_sum() <= 21 && dealer.get_hands()[0]->get_sum() > 21)
        {
            ui->Player1_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[0].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[0].won_round(players[0].get_hands()[0]->get_bet());
        }
        else if (players[0].get_hands()[0]->get_sum() == dealer.get_hands()[0]->get_sum())
        {
            ui->Player1_hand1_result->setText(QString::fromStdString("push (no gain/loss)"));
        }
        else
        {
            ui->Player1_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[0].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[0].won_round(players[0].get_hands()[0]->get_bet());
        }
        if (players[0].get_hands().size() > 1)
        {
            if (players[0].get_hands()[1]->get_sum() > 21)
            {
                ui->Player1_hand2_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[0].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[0].lost_round(players[0].get_hands()[1]->get_bet());
            }
            else if (players[0].get_hands()[1]->get_sum() < dealer.get_hands()[1]->get_sum() && dealer.get_hands()[1]->get_sum() <= 21)
            {
                ui->Player1_hand2_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[0].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[0].lost_round(players[0].get_hands()[1]->get_bet());
            }
            else if (players[0].get_hands()[1]->get_sum() <= 21 && dealer.get_hands()[1]->get_sum() > 21)
            {
                ui->Player1_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[0].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[0].won_round(players[0].get_hands()[1]->get_bet());
            }
            else if (players[0].get_hands()[1]->get_sum() == dealer.get_hands()[1]->get_sum())
            {
                ui->Player1_hand2_result->setText(QString::fromStdString("push (no gain/loss)"));
            }
            else
            {
                ui->Player1_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[0].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[0].won_round(players[0].get_hands()[1]->get_bet());
            }
        }
        ui->Player1_Result->setText(QString::fromStdString(players[0].get_name() + " ($") + QString::number(players[0].get_money()) + QString::fromStdString(")"));
    }
    if (players.size() >= 2)
    {
        if (players[1].get_hands()[0]->get_sum() > 21)
        {
            ui->Player2_hand1_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[1].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[1].lost_round(players[1].get_hands()[0]->get_bet());
        }
        else if (players[1].get_hands()[0]->get_sum() < dealer.get_hands()[0]->get_sum() && dealer.get_hands()[0]->get_sum() <= 21)
        {
            ui->Player2_hand1_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[1].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[1].lost_round(players[1].get_hands()[0]->get_bet());
        }
        else if (players[1].get_hands()[0]->get_sum() <= 21 && dealer.get_hands()[0]->get_sum() > 21)
        {
            ui->Player2_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[1].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[1].won_round(players[1].get_hands()[0]->get_bet());
        }
        else if (players[1].get_hands()[0]->get_sum() == dealer.get_hands()[0]->get_sum())
        {
            ui->Player2_hand1_result->setText(QString::fromStdString("push (no gain/loss)"));
        }
        else
        {
            ui->Player2_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[1].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[1].won_round(players[1].get_hands()[0]->get_bet());
        }
        if (players[1].get_hands().size() > 1)
        {
            if (players[1].get_hands()[1]->get_sum() > 21)
            {
                ui->Player2_hand2_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[1].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[1].lost_round(players[1].get_hands()[1]->get_bet());
            }
            else if (players[1].get_hands()[1]->get_sum() < dealer.get_hands()[1]->get_sum() && dealer.get_hands()[1]->get_sum() <= 21)
            {
                ui->Player2_hand2_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[1].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[1].lost_round(players[1].get_hands()[1]->get_bet());
            }
            else if (players[1].get_hands()[1]->get_sum() <= 21 && dealer.get_hands()[1]->get_sum() > 21)
            {
                ui->Player2_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[1].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[1].won_round(players[1].get_hands()[1]->get_bet());
            }
            else if (players[1].get_hands()[1]->get_sum() == dealer.get_hands()[1]->get_sum())
            {
                ui->Player2_hand2_result->setText(QString::fromStdString("push (no gain/loss)"));
            }
            else
            {
                ui->Player2_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[1].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[1].won_round(players[1].get_hands()[1]->get_bet());
            }
        }
        ui->Player2_Result->setText(QString::fromStdString(players[1].get_name() + " ($") + QString::number(players[1].get_money()) + QString::fromStdString(")"));
    }
    if (players.size() >= 3)
    {
        if (players[2].get_hands()[0]->get_sum() > 21)
        {
            ui->Player3_hand1_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[2].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[2].lost_round(players[2].get_hands()[0]->get_bet());
        }
        else if (players[2].get_hands()[0]->get_sum() < dealer.get_hands()[0]->get_sum() && dealer.get_hands()[0]->get_sum() <= 21)
        {
            ui->Player3_hand1_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[2].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[2].lost_round(players[2].get_hands()[0]->get_bet());
        }
        else if (players[2].get_hands()[0]->get_sum() <= 21 && dealer.get_hands()[0]->get_sum() > 21)
        {
            ui->Player3_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[2].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[2].won_round(players[2].get_hands()[0]->get_bet());
        }
        else if (players[2].get_hands()[0]->get_sum() == dealer.get_hands()[0]->get_sum())
        {
            ui->Player3_hand1_result->setText(QString::fromStdString("push (no gain/loss)"));
        }
        else
        {
            ui->Player3_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[2].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[2].won_round(players[2].get_hands()[0]->get_bet());
        }
        if (players[2].get_hands().size() > 1)
        {
            if (players[2].get_hands()[1]->get_sum() > 21)
            {
                ui->Player3_hand2_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[2].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[2].lost_round(players[2].get_hands()[1]->get_bet());
            }
            else if (players[2].get_hands()[1]->get_sum() < dealer.get_hands()[1]->get_sum() && dealer.get_hands()[1]->get_sum() <= 21)
            {
                ui->Player3_hand2_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[2].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[2].lost_round(players[2].get_hands()[1]->get_bet());
            }
            else if (players[2].get_hands()[1]->get_sum() <= 21 && dealer.get_hands()[1]->get_sum() > 21)
            {
                ui->Player3_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[2].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[2].won_round(players[2].get_hands()[1]->get_bet());
            }
            else if (players[2].get_hands()[1]->get_sum() == dealer.get_hands()[1]->get_sum())
            {
                ui->Player3_hand2_result->setText(QString::fromStdString("push (no gain/loss)"));
            }
            else
            {
                ui->Player3_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[2].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[2].won_round(players[2].get_hands()[1]->get_bet());
            }
        }
        ui->Player3_Result->setText(QString::fromStdString(players[2].get_name() + " ($") + QString::number(players[2].get_money()) + QString::fromStdString(")"));
    }
    if (players.size() >= 4)
    {
        if (players[3].get_hands()[0]->get_sum() > 21)
        {
            ui->Player4_hand1_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[3].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[3].lost_round(players[3].get_hands()[0]->get_bet());
        }
        else if (players[3].get_hands()[0]->get_sum() < dealer.get_hands()[0]->get_sum() && dealer.get_hands()[0]->get_sum() <= 21)
        {
            ui->Player4_hand1_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[3].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[3].lost_round(players[3].get_hands()[0]->get_bet());
        }
        else if (players[3].get_hands()[0]->get_sum() <= 21 && dealer.get_hands()[0]->get_sum() > 21)
        {
            ui->Player4_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[3].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[3].won_round(players[3].get_hands()[0]->get_bet());
        }
        else if (players[3].get_hands()[0]->get_sum() == dealer.get_hands()[0]->get_sum())
        {
            ui->Player4_hand1_result->setText(QString::fromStdString("push (no gain/loss)"));
        }
        else
        {
            ui->Player4_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[3].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[3].won_round(players[3].get_hands()[0]->get_bet());
        }
        if (players[3].get_hands().size() > 1)
        {
            if (players[3].get_hands()[1]->get_sum() > 21)
            {
                ui->Player4_hand2_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[3].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[3].lost_round(players[3].get_hands()[1]->get_bet());
            }
            else if (players[3].get_hands()[1]->get_sum() < dealer.get_hands()[1]->get_sum() && dealer.get_hands()[1]->get_sum() <= 21)
            {
                ui->Player4_hand2_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[3].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[3].lost_round(players[3].get_hands()[1]->get_bet());
            }
            else if (players[3].get_hands()[1]->get_sum() <= 21 && dealer.get_hands()[1]->get_sum() > 21)
            {
                ui->Player4_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[3].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[3].won_round(players[3].get_hands()[1]->get_bet());
            }
            else if (players[3].get_hands()[1]->get_sum() == dealer.get_hands()[1]->get_sum())
            {
                ui->Player4_hand2_result->setText(QString::fromStdString("push (no gain/loss)"));
            }
            else
            {
                ui->Player4_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[3].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[3].won_round(players[3].get_hands()[1]->get_bet());
            }
        }
        ui->Player4_Result->setText(QString::fromStdString(players[3].get_name() + " ($") + QString::number(players[3].get_money()) + QString::fromStdString(")"));
    }
    if (players.size() >= 5)
    {
        if (players[4].get_hands()[0]->get_sum() > 21)
        {
            ui->Player5_hand1_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[4].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[4].lost_round(players[4].get_hands()[0]->get_bet());
        }
        else if (players[4].get_hands()[0]->get_sum() < dealer.get_hands()[0]->get_sum() && dealer.get_hands()[0]->get_sum() <= 21)
        {
            ui->Player5_hand1_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[4].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[4].lost_round(players[4].get_hands()[0]->get_bet());
        }
        else if (players[4].get_hands()[0]->get_sum() <= 21 && dealer.get_hands()[0]->get_sum() > 21)
        {
            ui->Player5_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[4].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[4].won_round(players[4].get_hands()[0]->get_bet());
        }
        else if (players[4].get_hands()[0]->get_sum() == dealer.get_hands()[0]->get_sum())
        {
            ui->Player5_hand1_result->setText(QString::fromStdString("push (no gain/loss)"));
        }
        else
        {
            ui->Player5_hand1_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[4].get_hands()[0]->get_bet()) + QString::fromStdString(")"));
            players[4].won_round(players[4].get_hands()[0]->get_bet());
        }
        if (players[4].get_hands().size() > 1)
        {
            if (players[4].get_hands()[1]->get_sum() > 21)
            {
                ui->Player5_hand2_result->setText(QString::fromStdString("busted (lose $") + QString::number(players[4].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[4].lost_round(players[4].get_hands()[1]->get_bet());
            }
            else if (players[4].get_hands()[1]->get_sum() < dealer.get_hands()[1]->get_sum() && dealer.get_hands()[1]->get_sum() <= 21)
            {
                ui->Player5_hand2_result->setText(QString::fromStdString("lost to dealer (lose $") + QString::number(players[4].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[4].lost_round(players[4].get_hands()[1]->get_bet());
            }
            else if (players[4].get_hands()[1]->get_sum() <= 21 && dealer.get_hands()[1]->get_sum() > 21)
            {
                ui->Player5_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[4].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[4].won_round(players[4].get_hands()[1]->get_bet());
            }
            else if (players[4].get_hands()[1]->get_sum() == dealer.get_hands()[1]->get_sum())
            {
                ui->Player5_hand2_result->setText(QString::fromStdString("push (no gain/loss)"));
            }
            else
            {
                ui->Player5_hand2_result->setText(QString::fromStdString("beat dealer (win $") + QString::number(players[4].get_hands()[1]->get_bet()) + QString::fromStdString(")"));
                players[4].won_round(players[4].get_hands()[1]->get_bet());
            }
        }
        ui->Player5_Result->setText(QString::fromStdString(players[4].get_name() + " ($") + QString::number(players[4].get_money()) + QString::fromStdString(")"));
    }
}

void Final_Project::on_pushButton_3_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    for (int i = 0; i < players.size(); i++)
    {
        while (players[i].get_hands().size() > 1)
            players[i].get_hands().pop_back();
        while (players[i].get_hands()[0]->get_size() > 0)
            players[i].get_hands()[0]->remove_last_card();
    }
    deck = Deck();
}

void Final_Project::on_Big_blind_valueChanged(int arg1)
{
    ui->Small_blind_name->setText(QString::fromStdString(players[0].get_name() + " (Deducted $") + QString::number(ui->Big_blind->value()/2) + QString::fromStdString(")"));
    ui->Big_blind_name->setText(QString::fromStdString(players[1].get_name() + " (Deducted $") + QString::number(ui->Big_blind->value()) + QString::fromStdString(")"));
    current_bet = arg1;
}

void Final_Project::on_pushButton_5_clicked()
{
    srand((int)time(0));
    deck.shuffle();

    players[0].get_hands()[0]->set_bet(ui->Big_blind->value()/2);
    players[1].get_hands()[0]->set_bet(ui->Big_blind->value());

    ui->Max_bet_2->setText(QString::number(max_bet));
    ui->Current_bet->setText(QString::number(current_bet));
    ui->stackedWidget->setCurrentIndex(6);

    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < players.size(); j++)
        {
            players[j].get_hands()[0]->add_card(deck.deal_card());
        }
    }

    for (int i = 0; i < players.size(); i++)
    {
        ui->Choose_players_cards->insertItem(i + 1, QString::fromStdString(players[i].get_name()));
    }


    if (players.size() >= 2)
    {
        ui->Player1_name_2->setText(QString::fromStdString(players[0].get_name()));
        ui->Player1_check->setEnabled(true);
        ui->Player1_fold->setEnabled(true);
        ui->Player1_raise->setEnabled(true);

        ui->Player2_name_2->setText(QString::fromStdString(players[1].get_name()));
        ui->Player2_check->setEnabled(true);
        ui->Player2_fold->setEnabled(true);
        ui->Player2_raise->setEnabled(true);
    }
    if (players.size() >= 3)
    {
        ui->Player3_name_2->setText(QString::fromStdString(players[2].get_name()));
        ui->Player3_check->setEnabled(true);
        ui->Player3_fold->setEnabled(true);
        ui->Player3_raise->setEnabled(true);
    }
    if (players.size() >= 4)
    {
        ui->Player4_name_2->setText(QString::fromStdString(players[3].get_name()));
        ui->Player4_check->setEnabled(true);
        ui->Player4_fold->setEnabled(true);
        ui->Player4_raise->setEnabled(true);
    }
    if (players.size() == 5)
    {
        ui->Player5_name_2->setText(QString::fromStdString(players[4].get_name()));
        ui->Player5_check->setEnabled(true);
        ui->Player5_fold->setEnabled(true);
        ui->Player5_raise->setEnabled(true);
    }
}


void Final_Project::on_Player1_check_clicked()
{
    players[0].get_hands()[0]->set_bet(current_bet);
    ui->Player1_check->setEnabled(false);
    ui->Player1_fold->setEnabled(false);
    ui->Player1_raise->setEnabled(false);
}

void Final_Project::on_Player1_fold_clicked()
{
    players[0].change_status(false);

    ui->Player1_check->setEnabled(false);
    ui->Player1_fold->setEnabled(false);
    ui->Player1_raise->setEnabled(false);
}

void Final_Project::on_Player1_raise_clicked()
{
    ui->label_28->setEnabled(true);
    ui->Player1_new_bet->setEnabled(true);

    ui->Player1_check->setEnabled(false);
    ui->Player1_fold->setEnabled(false);
    ui->Player1_raise->setEnabled(false);
    ui->Player1_new_bet->setMinimum(current_bet + 1);
}

void Final_Project::on_Player1_new_bet_valueChanged(int arg1)
{
    players[0].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet->setText(QString::number(current_bet));
}

void Final_Project::on_Player2_check_clicked()
{
    players[1].get_hands()[0]->set_bet(current_bet);
    ui->Player2_check->setEnabled(false);
    ui->Player2_fold->setEnabled(false);
    ui->Player2_raise->setEnabled(false);
    ui->label_28->setEnabled(false);
    ui->Player1_new_bet->setEnabled(false);
}

void Final_Project::on_Player2_fold_clicked()
{
    players[1].change_status(false);

    ui->Player2_check->setEnabled(false);
    ui->Player2_fold->setEnabled(false);
    ui->Player2_raise->setEnabled(false);
    ui->label_28->setEnabled(false);
    ui->Player1_new_bet->setEnabled(false);
}

void Final_Project::on_Player2_raise_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check->setEnabled(true);
        ui->Player1_fold->setEnabled(true);
    }

    ui->Player2_check->setEnabled(false);
    ui->Player2_fold->setEnabled(false);
    ui->Player2_raise->setEnabled(false);
    ui->Player2_new_bet->setEnabled(true);
    ui->Player2_new_bet->setMinimum(current_bet + 1);
    ui->label_29->setEnabled(true);
    ui->label_28->setEnabled(false);
    ui->Player1_new_bet->setEnabled(false);
}

void Final_Project::on_Player2_new_bet_valueChanged(int arg1)
{
    players[1].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet->setText(QString::number(current_bet));
}

void Final_Project::on_Player3_check_clicked()
{
    players[2].get_hands()[0]->set_bet(current_bet);
    ui->Player3_check->setEnabled(false);
    ui->Player3_fold->setEnabled(false);
    ui->Player3_raise->setEnabled(false);
    ui->label_29->setEnabled(false);
    ui->Player2_new_bet->setEnabled(false);
}

void Final_Project::on_Player3_fold_clicked()
{
    players[2].change_status(false);

    ui->Player3_check->setEnabled(false);
    ui->Player3_fold->setEnabled(false);
    ui->Player3_raise->setEnabled(false);
    ui->label_29->setEnabled(false);
    ui->Player2_new_bet->setEnabled(false);
}

void Final_Project::on_Player3_raise_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check->setEnabled(true);
        ui->Player1_fold->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check->setEnabled(true);
        ui->Player2_fold->setEnabled(true);
    }

    ui->Player3_check->setEnabled(false);
    ui->Player3_fold->setEnabled(false);
    ui->Player3_raise->setEnabled(false);
    ui->Player3_new_bet->setEnabled(true);
    ui->Player3_new_bet->setMinimum(current_bet + 1);
    ui->label_30->setEnabled(true);
    ui->label_29->setEnabled(false);
    ui->Player2_new_bet->setEnabled(false);
}

void Final_Project::on_Player3_new_bet_valueChanged(int arg1)
{
    players[2].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet->setText(QString::number(current_bet));
}

void Final_Project::on_Player4_check_clicked()
{
    players[3].get_hands()[0]->set_bet(current_bet);
    ui->Player4_check->setEnabled(false);
    ui->Player4_fold->setEnabled(false);
    ui->Player4_raise->setEnabled(false);
    ui->label_30->setEnabled(false);
    ui->Player3_new_bet->setEnabled(false);
}

void Final_Project::on_Player4_fold_clicked()
{
    players[3].change_status(false);

    ui->Player4_check->setEnabled(false);
    ui->Player4_fold->setEnabled(false);
    ui->Player4_raise->setEnabled(false);
    ui->label_30->setEnabled(false);
    ui->Player3_new_bet->setEnabled(false);
}

void Final_Project::on_Player4_raise_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check->setEnabled(true);
        ui->Player1_fold->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check->setEnabled(true);
        ui->Player2_fold->setEnabled(true);
    }
    if (players[2].get_status() == true)
    {
        ui->Player3_check->setEnabled(true);
        ui->Player3_fold->setEnabled(true);
    }

    ui->Player4_check->setEnabled(false);
    ui->Player4_fold->setEnabled(false);
    ui->Player4_raise->setEnabled(false);
    ui->Player4_new_bet->setEnabled(true);
    ui->Player4_new_bet->setMinimum(current_bet + 1);
    ui->label_31->setEnabled(true);
    ui->label_30->setEnabled(false);
    ui->Player3_new_bet->setEnabled(false);
}

void Final_Project::on_Player4_new_bet_valueChanged(int arg1)
{
    players[3].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet->setText(QString::number(current_bet));
}

void Final_Project::on_Player5_check_clicked()
{
    players[4].get_hands()[0]->set_bet(current_bet);
    ui->Player5_check->setEnabled(false);
    ui->Player5_fold->setEnabled(false);
    ui->Player5_raise->setEnabled(false);
    ui->label_31->setEnabled(false);
    ui->Player4_new_bet->setEnabled(false);
}

void Final_Project::on_Player5_fold_clicked()
{
    players[4].change_status(false);

    ui->Player4_check->setEnabled(false);
    ui->Player4_fold->setEnabled(false);
    ui->Player4_raise->setEnabled(false);
    ui->label_31->setEnabled(false);
    ui->Player4_new_bet->setEnabled(false);
}

void Final_Project::on_Player5_raise_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check->setEnabled(true);
        ui->Player1_fold->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check->setEnabled(true);
        ui->Player2_fold->setEnabled(true);
    }
    if (players[2].get_status() == true)
    {
        ui->Player3_check->setEnabled(true);
        ui->Player3_fold->setEnabled(true);
    }
    if (players[3].get_status() == true)
    {
        ui->Player4_check->setEnabled(true);
        ui->Player4_fold->setEnabled(true);
    }

    ui->Player5_check->setEnabled(false);
    ui->Player5_fold->setEnabled(false);
    ui->Player5_raise->setEnabled(false);
    ui->Player5_new_bet->setEnabled(true);
    ui->Player5_new_bet->setMinimum(current_bet + 1);
    ui->label_31->setEnabled(true);
    ui->label_30->setEnabled(false);
    ui->Player4_new_bet->setEnabled(false);
}

void Final_Project::on_Player5_new_bet_valueChanged(int arg1)
{
    players[4].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet->setText(QString::number(current_bet));
}

void Final_Project::on_Choose_players_cards_currentIndexChanged(int index)
{
    if (index == 0)
    {
        ui->Players_cards->setText(QString::fromStdString(""));
    }
    if (index == 1)
    {
        ui->Players_cards->setText(QString::fromStdString(players[0].print_hand(0)));
    }
    if (index == 2)
    {
        ui->Players_cards->setText(QString::fromStdString(players[1].print_hand(0)));
    }
    if (index == 3)
    {
        ui->Players_cards->setText(QString::fromStdString(players[2].print_hand(0)));
    }
    if (index == 4)
    {
        ui->Players_cards->setText(QString::fromStdString(players[3].print_hand(0)));
    }
    if (index == 5)
    {
        ui->Players_cards->setText(QString::fromStdString(players[4].print_hand(0)));
    }
}

void Final_Project::on_pushButton_6_clicked()
{
    ui->Max_bet_3->setText(QString::number(max_bet));
    ui->Current_bet_2->setText(QString::number(current_bet));
    ui->stackedWidget->setCurrentIndex(7);

    discard.add_card(deck.deal_card());

    play.add_card(deck.deal_card());
    play.add_card(deck.deal_card());
    play.add_card(deck.deal_card());

    ui->Flop->setText(QString::fromStdString(play.print_cards()));

    for (int i = 0; i < players.size(); i++)
    {
        if (players[i].get_status() == true)
            ui->Choose_players_cards_2->insertItem(i + 1, QString::fromStdString(players[i].get_name()));
    }

    if (players.size() >= 1 && players[0].get_status() == true)
    {
        ui->Player1_name_3->setText(QString::fromStdString(players[0].get_name()));
        ui->Player1_check_2->setEnabled(true);
        ui->Player1_fold_2->setEnabled(true);
        ui->Player1_raise_2->setEnabled(true);
    }

    if (players.size() >= 2 && players[1].get_status() == true)
    {
        ui->Player2_name_3->setText(QString::fromStdString(players[1].get_name()));
        ui->Player2_check_2->setEnabled(true);
        ui->Player2_fold_2->setEnabled(true);
        ui->Player2_raise_2->setEnabled(true);
    }
    if (players.size() >= 3 && players[2].get_status() == true)
    {
        ui->Player3_name_3->setText(QString::fromStdString(players[2].get_name()));
        ui->Player3_check_2->setEnabled(true);
        ui->Player3_fold_2->setEnabled(true);
        ui->Player3_raise_2->setEnabled(true);
    }
    if (players.size() >= 4 && players[3].get_status() == true)
    {
        ui->Player4_name_3->setText(QString::fromStdString(players[3].get_name()));
        ui->Player4_check_2->setEnabled(true);
        ui->Player4_fold_2->setEnabled(true);
        ui->Player4_raise_2->setEnabled(true);
    }
    if (players.size() == 5 && players[4].get_status() == true)
    {
        ui->Player5_name_3->setText(QString::fromStdString(players[4].get_name()));
        ui->Player5_check_2->setEnabled(true);
        ui->Player5_fold_2->setEnabled(true);
        ui->Player5_raise_2->setEnabled(true);
    }
}

void Final_Project::on_Player1_check_2_clicked()
{
    players[0].get_hands()[0]->set_bet(current_bet);
    ui->Player1_check_2->setEnabled(false);
    ui->Player1_fold_2->setEnabled(false);
    ui->Player1_raise_2->setEnabled(false);
}

void Final_Project::on_Player1_fold_2_clicked()
{
    players[0].change_status(false);

    ui->Player1_check_2->setEnabled(false);
    ui->Player1_fold_2->setEnabled(false);
    ui->Player1_raise_2->setEnabled(false);
}

void Final_Project::on_Player1_raise_2_clicked()
{
    ui->label_35->setEnabled(true);
    ui->Player1_new_bet_2->setEnabled(true);

    ui->Player1_check_2->setEnabled(false);
    ui->Player1_fold_2->setEnabled(false);
    ui->Player1_raise_2->setEnabled(false);
    ui->Player1_new_bet_2->setMinimum(current_bet + 1);
}

void Final_Project::on_Player1_new_bet_2_valueChanged(int arg1)
{
    players[0].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_2->setText(QString::number(current_bet));
}



void Final_Project::on_Player2_check_2_clicked()
{
    players[1].get_hands()[0]->set_bet(current_bet);
    ui->Player2_check_2->setEnabled(false);
    ui->Player2_fold_2->setEnabled(false);
    ui->Player2_raise_2->setEnabled(false);
    ui->label_35->setEnabled(false);
    ui->Player1_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player2_fold_2_clicked()
{
    players[1].change_status(false);

    ui->Player2_check_2->setEnabled(false);
    ui->Player2_fold_2->setEnabled(false);
    ui->Player2_raise_2->setEnabled(false);
    ui->label_34->setEnabled(false);
    ui->Player1_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player2_raise_2_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_2->setEnabled(true);
        ui->Player1_fold_2->setEnabled(true);
    }

    ui->Player2_check_2->setEnabled(false);
    ui->Player2_fold_2->setEnabled(false);
    ui->Player2_raise_2->setEnabled(false);
    ui->Player2_new_bet_2->setEnabled(true);
    ui->Player2_new_bet_2->setMinimum(current_bet + 1);
    ui->label_34->setEnabled(true);
    ui->label_35->setEnabled(false);
    ui->Player1_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player2_new_bet_2_valueChanged(int arg1)
{
    players[1].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_2->setText(QString::number(current_bet));
}

void Final_Project::on_Player3_check_2_clicked()
{
    players[2].get_hands()[0]->set_bet(current_bet);
    ui->Player3_check_2->setEnabled(false);
    ui->Player3_fold_2->setEnabled(false);
    ui->Player3_raise_2->setEnabled(false);
    ui->label_34->setEnabled(false);
    ui->Player2_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player3_fold_2_clicked()
{
    players[2].change_status(false);

    ui->Player3_check_2->setEnabled(false);
    ui->Player3_fold_2->setEnabled(false);
    ui->Player3_raise_2->setEnabled(false);
    ui->label_34->setEnabled(false);
    ui->Player2_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player3_raise_2_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_2->setEnabled(true);
        ui->Player1_fold_2->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check_2->setEnabled(true);
        ui->Player2_fold_2->setEnabled(true);
    }

    ui->Player3_check_2->setEnabled(false);
    ui->Player3_fold_2->setEnabled(false);
    ui->Player3_raise_2->setEnabled(false);
    ui->Player3_new_bet_2->setEnabled(true);
    ui->Player3_new_bet_2->setMinimum(current_bet + 1);
    ui->label_36->setEnabled(true);
    ui->label_34->setEnabled(false);
    ui->Player2_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player3_new_bet_2_valueChanged(int arg1)
{
    players[2].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_2->setText(QString::number(current_bet));
}

void Final_Project::on_Player4_check_2_clicked()
{
    players[3].get_hands()[0]->set_bet(current_bet);
    ui->Player4_check_2->setEnabled(false);
    ui->Player4_fold_2->setEnabled(false);
    ui->Player4_raise_2->setEnabled(false);
    ui->label_36->setEnabled(false);
    ui->Player3_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player4_fold_2_clicked()
{
    players[3].change_status(false);

    ui->Player4_check_2->setEnabled(false);
    ui->Player4_fold_2->setEnabled(false);
    ui->Player4_raise_2->setEnabled(false);
    ui->label_36->setEnabled(false);
    ui->Player3_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player4_raise_2_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_2->setEnabled(true);
        ui->Player1_fold_2->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check_2->setEnabled(true);
        ui->Player2_fold_2->setEnabled(true);
    }
    if (players[2].get_status() == true)
    {
        ui->Player3_check_2->setEnabled(true);
        ui->Player3_fold_2->setEnabled(true);
    }

    ui->Player4_check_2->setEnabled(false);
    ui->Player4_fold_2->setEnabled(false);
    ui->Player4_raise_2->setEnabled(false);
    ui->Player4_new_bet_2->setEnabled(true);
    ui->Player4_new_bet_2->setMinimum(current_bet + 1);
    ui->label_37->setEnabled(true);
    ui->label_36->setEnabled(false);
    ui->Player3_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player4_new_bet_2_valueChanged(int arg1)
{
    players[3].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_2->setText(QString::number(current_bet));
}

void Final_Project::on_Player5_check_2_clicked()
{
    players[4].get_hands()[0]->set_bet(current_bet);
    ui->Player5_check_2->setEnabled(false);
    ui->Player5_fold_2->setEnabled(false);
    ui->Player5_raise_2->setEnabled(false);
    ui->label_37->setEnabled(false);
    ui->Player4_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player5_fold_2_clicked()
{
    players[4].change_status(false);

    ui->Player4_check_2->setEnabled(false);
    ui->Player4_fold_2->setEnabled(false);
    ui->Player4_raise_2->setEnabled(false);
    ui->label_37->setEnabled(false);
    ui->Player4_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player5_raise_2_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_2->setEnabled(true);
        ui->Player1_fold_2->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check_2->setEnabled(true);
        ui->Player2_fold_2->setEnabled(true);
    }
    if (players[2].get_status() == true)
    {
        ui->Player3_check_2->setEnabled(true);
        ui->Player3_fold_2->setEnabled(true);
    }
    if (players[3].get_status() == true)
    {
        ui->Player4_check_2->setEnabled(true);
        ui->Player4_fold_2->setEnabled(true);
    }

    ui->Player5_check_2->setEnabled(false);
    ui->Player5_fold_2->setEnabled(false);
    ui->Player5_raise_2->setEnabled(false);
    ui->Player5_new_bet_2->setEnabled(true);
    ui->Player5_new_bet_2->setMinimum(current_bet + 1);
    ui->label_38->setEnabled(true);
    ui->label_37->setEnabled(false);
    ui->Player4_new_bet_2->setEnabled(false);
}

void Final_Project::on_Player5_new_bet_2_valueChanged(int arg1)
{
    players[4].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_2->setText(QString::number(current_bet));
}

void Final_Project::on_Choose_players_cards_2_currentIndexChanged(int index)
{
    if (index == 0)
    {
        ui->Players_cards_2->setText(QString::fromStdString(""));
    }
    if (index == 1)
    {
        ui->Players_cards_2->setText(QString::fromStdString(players[0].print_hand(0)));
    }
    if (index == 2)
    {
        ui->Players_cards_2->setText(QString::fromStdString(players[1].print_hand(0)));
    }
    if (index == 3)
    {
        ui->Players_cards_2->setText(QString::fromStdString(players[2].print_hand(0)));
    }
    if (index == 4)
    {
        ui->Players_cards_2->setText(QString::fromStdString(players[3].print_hand(0)));
    }
    if (index == 5)
    {
        ui->Players_cards_2->setText(QString::fromStdString(players[4].print_hand(0)));
    }
}

void Final_Project::on_pushButton_7_clicked()
{
    ui->stackedWidget->setCurrentIndex(8);
    discard.add_card(deck.deal_card());
    play.add_card(deck.deal_card());
    ui->Max_bet_4->setText(QString::number(max_bet));
    ui->Current_bet_3->setText(QString::number(current_bet));
    ui->Turn->setText(QString::fromStdString(play.print_cards()));
    for (int i = 0; i < players.size(); i++)
    {
        if (players[i].get_status() == true)
            ui->Choose_players_cards_3->insertItem(i + 1, QString::fromStdString(players[i].get_name()));
    }

    if (players.size() >= 1 && players[0].get_status() == true)
    {
        ui->Player1_name_4->setText(QString::fromStdString(players[0].get_name()));
        ui->Player1_check_3->setEnabled(true);
        ui->Player1_fold_3->setEnabled(true);
        ui->Player1_raise_3->setEnabled(true);
    }
    if(players.size() >= 2 && players[1].get_status() == true)
    {
        ui->Player2_name_4->setText(QString::fromStdString(players[1].get_name()));
        ui->Player2_check_3->setEnabled(true);
        ui->Player2_fold_3->setEnabled(true);
        ui->Player2_raise_3->setEnabled(true);
    }
    if (players.size() >= 3 && players[2].get_status() == true)
    {
        ui->Player3_name_4->setText(QString::fromStdString(players[2].get_name()));
        ui->Player3_check_3->setEnabled(true);
        ui->Player3_fold_3->setEnabled(true);
        ui->Player3_raise_3->setEnabled(true);
    }
    if (players.size() >= 4 && players[3].get_status() == true)
    {
        ui->Player4_name_4->setText(QString::fromStdString(players[3].get_name()));
        ui->Player4_check_3->setEnabled(true);
        ui->Player4_fold_3->setEnabled(true);
        ui->Player4_raise_3->setEnabled(true);
    }
    if (players.size() == 5 && players[4].get_status() == true)
    {
        ui->Player5_name_4->setText(QString::fromStdString(players[4].get_name()));
        ui->Player5_check_3->setEnabled(true);
        ui->Player5_fold_3->setEnabled(true);
        ui->Player5_raise_3->setEnabled(true);
    }
}

void Final_Project::on_Player1_check_3_clicked()
{
    players[0].get_hands()[0]->set_bet(current_bet);
    ui->Player1_check_3->setEnabled(false);
    ui->Player1_fold_3->setEnabled(false);
    ui->Player1_raise_3->setEnabled(false);
}

void Final_Project::on_Player1_fold_3_clicked()
{
    players[0].change_status(false);

    ui->Player1_check_3->setEnabled(false);
    ui->Player1_fold_3->setEnabled(false);
    ui->Player1_raise_3->setEnabled(false);
}

void Final_Project::on_Player1_raise_3_clicked()
{
    ui->label_41->setEnabled(true);
    ui->Player1_new_bet_3->setEnabled(true);

    ui->Player1_check_3->setEnabled(false);
    ui->Player1_fold_3->setEnabled(false);
    ui->Player1_raise_3->setEnabled(false);
    ui->Player1_new_bet_3->setMinimum(current_bet + 1);
}

void Final_Project::on_Player1_new_bet_3_valueChanged(int arg1)
{
    players[0].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_3->setText(QString::number(current_bet));
}

void Final_Project::on_Player2_check_3_clicked()
{
    players[1].get_hands()[0]->set_bet(current_bet);
    ui->Player2_check_3->setEnabled(false);
    ui->Player2_fold_3->setEnabled(false);
    ui->Player2_raise_3->setEnabled(false);
    ui->label_41->setEnabled(false);
    ui->Player1_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player2_fold_3_clicked()
{
    players[1].change_status(false);

    ui->Player2_check_3->setEnabled(false);
    ui->Player2_fold_3->setEnabled(false);
    ui->Player2_raise_3->setEnabled(false);
    ui->label_41->setEnabled(false);
    ui->Player1_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player2_raise_3_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_3->setEnabled(true);
        ui->Player1_fold_3->setEnabled(true);
    }

    ui->Player2_check_3->setEnabled(false);
    ui->Player2_fold_3->setEnabled(false);
    ui->Player2_raise_3->setEnabled(false);
    ui->Player2_new_bet_3->setEnabled(true);
    ui->Player2_new_bet_3->setMinimum(current_bet + 1);
    ui->label_42->setEnabled(true);
    ui->label_41->setEnabled(false);
    ui->Player1_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player2_new_bet_3_valueChanged(int arg1)
{
    players[1].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_3->setText(QString::number(current_bet));
}

void Final_Project::on_Player3_check_3_clicked()
{
    players[2].get_hands()[0]->set_bet(current_bet);
    ui->Player3_check_3->setEnabled(false);
    ui->Player3_fold_3->setEnabled(false);
    ui->Player3_raise_3->setEnabled(false);
    ui->label_42->setEnabled(false);
    ui->Player2_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player3_fold_3_clicked()
{
    players[2].change_status(false);

    ui->Player3_check_3->setEnabled(false);
    ui->Player3_fold_3->setEnabled(false);
    ui->Player3_raise_3->setEnabled(false);
    ui->label_42->setEnabled(false);
    ui->Player2_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player3_raise_3_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_3->setEnabled(true);
        ui->Player1_fold_3->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check_3->setEnabled(true);
        ui->Player2_fold_3->setEnabled(true);
    }

    ui->Player3_check_3->setEnabled(false);
    ui->Player3_fold_3->setEnabled(false);
    ui->Player3_raise_3->setEnabled(false);
    ui->Player3_new_bet_3->setEnabled(true);
    ui->Player3_new_bet_3->setMinimum(current_bet + 1);
    ui->label_43->setEnabled(true);
    ui->label_42->setEnabled(false);
    ui->Player2_new_bet->setEnabled(false);
}

void Final_Project::on_Player3_new_bet_3_valueChanged(int arg1)
{
    players[2].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_3->setText(QString::number(current_bet));
}

void Final_Project::on_Player4_check_3_clicked()
{
    players[3].get_hands()[0]->set_bet(current_bet);
    ui->Player4_check_3->setEnabled(false);
    ui->Player4_fold_3->setEnabled(false);
    ui->Player4_raise_3->setEnabled(false);
    ui->label_43->setEnabled(false);
    ui->Player3_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player4_fold_3_clicked()
{
    players[3].change_status(false);

    ui->Player4_check_3->setEnabled(false);
    ui->Player4_fold_3->setEnabled(false);
    ui->Player4_raise_3->setEnabled(false);
    ui->label_43->setEnabled(false);
    ui->Player3_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player4_raise_3_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_3->setEnabled(true);
        ui->Player1_fold_3->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check_3->setEnabled(true);
        ui->Player2_fold_3->setEnabled(true);
    }
    if (players[2].get_status() == true)
    {
        ui->Player3_check_3->setEnabled(true);
        ui->Player3_fold_3->setEnabled(true);
    }

    ui->Player4_check_3->setEnabled(false);
    ui->Player4_fold_3->setEnabled(false);
    ui->Player4_raise_3->setEnabled(false);
    ui->Player4_new_bet_3->setEnabled(true);
    ui->Player4_new_bet_3->setMinimum(current_bet + 1);
    ui->label_44->setEnabled(true);
    ui->label_43->setEnabled(false);
    ui->Player3_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player4_new_bet_3_valueChanged(int arg1)
{
    players[3].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_3->setText(QString::number(current_bet));
}

void Final_Project::on_Player5_check_3_clicked()
{
    players[4].get_hands()[0]->set_bet(current_bet);
    ui->Player5_check_3->setEnabled(false);
    ui->Player5_fold_3->setEnabled(false);
    ui->Player5_raise_3->setEnabled(false);
    ui->label_44->setEnabled(false);
    ui->Player4_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player5_fold_3_clicked()
{
    players[4].change_status(false);

    ui->Player4_check_3->setEnabled(false);
    ui->Player4_fold_3->setEnabled(false);
    ui->Player4_raise_3->setEnabled(false);
    ui->label_44->setEnabled(false);
    ui->Player4_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player5_raise_3_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_3->setEnabled(true);
        ui->Player1_fold_3->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check_3->setEnabled(true);
        ui->Player2_fold_3->setEnabled(true);
    }
    if (players[2].get_status() == true)
    {
        ui->Player3_check_3->setEnabled(true);
        ui->Player3_fold_3->setEnabled(true);
    }
    if (players[3].get_status() == true)
    {
        ui->Player4_check_3->setEnabled(true);
        ui->Player4_fold_3->setEnabled(true);
    }

    ui->Player5_check_3->setEnabled(false);
    ui->Player5_fold_3->setEnabled(false);
    ui->Player5_raise_3->setEnabled(false);
    ui->Player5_new_bet_3->setEnabled(true);
    ui->Player5_new_bet_3->setMinimum(current_bet + 1);
    ui->label_45->setEnabled(true);
    ui->label_44->setEnabled(false);
    ui->Player4_new_bet_3->setEnabled(false);
}

void Final_Project::on_Player5_new_bet_3_valueChanged(int arg1)
{
    players[4].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_3->setText(QString::number(current_bet));
}

void Final_Project::on_Choose_players_cards_3_currentIndexChanged(int index)
{
    if (index == 0)
    {
        ui->Players_cards_3->setText(QString::fromStdString(""));
    }
    if (index == 1)
    {
        ui->Players_cards_3->setText(QString::fromStdString(players[0].print_hand(0)));
    }
    if (index == 2)
    {
        ui->Players_cards_3->setText(QString::fromStdString(players[1].print_hand(0)));
    }
    if (index == 3)
    {
        ui->Players_cards_3->setText(QString::fromStdString(players[2].print_hand(0)));
    }
    if (index == 4)
    {
        ui->Players_cards_3->setText(QString::fromStdString(players[3].print_hand(0)));
    }
    if (index == 5)
    {
        ui->Players_cards_3->setText(QString::fromStdString(players[4].print_hand(0)));
    }
}


void Final_Project::on_pushButton_8_clicked()
{
    ui->stackedWidget->setCurrentIndex(9);
    discard.add_card(deck.deal_card());
    play.add_card(deck.deal_card());
    ui->Max_bet_5->setText(QString::number(max_bet));
    ui->Current_bet_4->setText(QString::number(current_bet));
    ui->Turn_2->setText(QString::fromStdString(play.print_cards()));
    for (int i = 0; i < players.size(); i++)
    {
        if (players[i].get_status() == true)
            ui->Choose_players_cards_4->insertItem(i + 1, QString::fromStdString(players[i].get_name()));
    }

    if (players.size() >= 1 && players[0].get_status() == true)
    {
        ui->Player1_name_5->setText(QString::fromStdString(players[0].get_name()));
        ui->Player1_check_4->setEnabled(true);
        ui->Player1_fold_4->setEnabled(true);
        ui->Player1_raise_4->setEnabled(true);
    }
    if (players.size() >= 2 && players[1].get_status() == true)
    {
        ui->Player2_name_5->setText(QString::fromStdString(players[1].get_name()));
        ui->Player2_check_4->setEnabled(true);
        ui->Player2_fold_4->setEnabled(true);
        ui->Player2_raise_4->setEnabled(true);
    }
    if (players.size() >= 3 && players[2].get_status() == true)
    {
        ui->Player3_name_5->setText(QString::fromStdString(players[2].get_name()));
        ui->Player3_check_4->setEnabled(true);
        ui->Player3_fold_4->setEnabled(true);
        ui->Player3_raise_4->setEnabled(true);
    }
    if (players.size() >= 4 && players[3].get_status() == true)
    {
        ui->Player4_name_5->setText(QString::fromStdString(players[3].get_name()));
        ui->Player4_check_4->setEnabled(true);
        ui->Player4_fold_4->setEnabled(true);
        ui->Player4_raise_4->setEnabled(true);
    }
    if (players.size() == 5 && players[4].get_status() == true)
    {
        ui->Player5_name_5->setText(QString::fromStdString(players[4].get_name()));
        ui->Player5_check_4->setEnabled(true);
        ui->Player5_fold_4->setEnabled(true);
        ui->Player5_raise_4->setEnabled(true);
    }
}

void Final_Project::on_Player1_check_4_clicked()
{
    players[0].get_hands()[0]->set_bet(current_bet);
    ui->Player1_check_4->setEnabled(false);
    ui->Player1_fold_4->setEnabled(false);
    ui->Player1_raise_4->setEnabled(false);
}

void Final_Project::on_Player1_fold_4_clicked()
{
    players[0].change_status(false);

    ui->Player1_check_4->setEnabled(false);
    ui->Player1_fold_4->setEnabled(false);
    ui->Player1_raise_4->setEnabled(false);
}

void Final_Project::on_Player1_raise_4_clicked()
{
    ui->label_48->setEnabled(true);
    ui->Player1_new_bet_4->setEnabled(true);

    ui->Player1_check_4->setEnabled(false);
    ui->Player1_fold_4->setEnabled(false);
    ui->Player1_raise_4->setEnabled(false);
    ui->Player1_new_bet_4->setMinimum(current_bet + 1);
}

void Final_Project::on_Player1_new_bet_4_valueChanged(int arg1)
{
    players[0].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_4->setText(QString::number(current_bet));
}


void Final_Project::on_Player2_check_4_clicked()
{
    players[1].get_hands()[0]->set_bet(current_bet);
    ui->Player2_check_4->setEnabled(false);
    ui->Player2_fold_4->setEnabled(false);
    ui->Player2_raise_4->setEnabled(false);
    ui->label_48->setEnabled(false);
    ui->Player1_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player2_fold_4_clicked()
{
    players[1].change_status(false);

    ui->Player2_check_4->setEnabled(false);
    ui->Player2_fold_4->setEnabled(false);
    ui->Player2_raise_4->setEnabled(false);
    ui->label_48->setEnabled(false);
    ui->Player1_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player2_raise_4_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_4->setEnabled(true);
        ui->Player1_fold_4->setEnabled(true);
    }

    ui->Player2_check_4->setEnabled(false);
    ui->Player2_fold_4->setEnabled(false);
    ui->Player2_raise_4->setEnabled(false);
    ui->Player2_new_bet_4->setEnabled(true);
    ui->Player2_new_bet_4->setMinimum(current_bet + 1);
    ui->label_49->setEnabled(true);
    ui->label_48->setEnabled(false);
    ui->Player1_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player2_new_bet_4_valueChanged(int arg1)
{
    players[1].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_4->setText(QString::number(current_bet));
}

void Final_Project::on_Player3_check_4_clicked()
{
    players[2].get_hands()[0]->set_bet(current_bet);
    ui->Player3_check_4->setEnabled(false);
    ui->Player3_fold_4->setEnabled(false);
    ui->Player3_raise_4->setEnabled(false);
    ui->label_49->setEnabled(false);
    ui->Player2_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player3_fold_4_clicked()
{
    players[2].change_status(false);

    ui->Player3_check_4->setEnabled(false);
    ui->Player3_fold_4->setEnabled(false);
    ui->Player3_raise_4->setEnabled(false);
    ui->label_49->setEnabled(false);
    ui->Player2_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player3_raise_4_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_4->setEnabled(true);
        ui->Player1_fold_4->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check_4->setEnabled(true);
        ui->Player2_fold_4->setEnabled(true);
    }

    ui->Player3_check_4->setEnabled(false);
    ui->Player3_fold_4->setEnabled(false);
    ui->Player3_raise_4->setEnabled(false);
    ui->Player3_new_bet_4->setEnabled(true);
    ui->Player3_new_bet_4->setMinimum(current_bet + 1);
    ui->label_50->setEnabled(true);
    ui->label_49->setEnabled(false);
    ui->Player2_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player3_new_bet_4_valueChanged(int arg1)
{
    players[2].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_4->setText(QString::number(current_bet));
}

void Final_Project::on_Player4_check_4_clicked()
{
    players[3].get_hands()[0]->set_bet(current_bet);
    ui->Player4_check_4->setEnabled(false);
    ui->Player4_fold_4->setEnabled(false);
    ui->Player4_raise_4->setEnabled(false);
    ui->label_50->setEnabled(false);
    ui->Player3_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player4_fold_4_clicked()
{
    players[3].change_status(false);

    ui->Player4_check_4->setEnabled(false);
    ui->Player4_fold_4->setEnabled(false);
    ui->Player4_raise_4->setEnabled(false);
    ui->label_50->setEnabled(false);
    ui->Player3_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player4_raise_4_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_4->setEnabled(true);
        ui->Player1_fold_4->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check_4->setEnabled(true);
        ui->Player2_fold_4->setEnabled(true);
    }
    if (players[2].get_status() == true)
    {
        ui->Player3_check_4->setEnabled(true);
        ui->Player3_fold_4->setEnabled(true);
    }

    ui->Player4_check_4->setEnabled(false);
    ui->Player4_fold_4->setEnabled(false);
    ui->Player4_raise_4->setEnabled(false);
    ui->Player4_new_bet_4->setEnabled(true);
    ui->Player4_new_bet_4->setMinimum(current_bet + 1);
    ui->label_51->setEnabled(true);
    ui->label_50->setEnabled(false);
    ui->Player3_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player4_new_bet_4_valueChanged(int arg1)
{
    players[3].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_4->setText(QString::number(current_bet));
}

void Final_Project::on_Player5_check_4_clicked()
{
    players[4].get_hands()[0]->set_bet(current_bet);
    ui->Player5_check_4->setEnabled(false);
    ui->Player5_fold_4->setEnabled(false);
    ui->Player5_raise_4->setEnabled(false);
    ui->label_51->setEnabled(false);
    ui->Player4_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player5_fold_4_clicked()
{
    players[4].change_status(false);

    ui->Player4_check_4->setEnabled(false);
    ui->Player4_fold_4->setEnabled(false);
    ui->Player4_raise_4->setEnabled(false);
    ui->label_51->setEnabled(false);
    ui->Player4_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player5_raise_4_clicked()
{
    if (players[0].get_status() == true)
    {
        ui->Player1_check_4->setEnabled(true);
        ui->Player1_fold_4->setEnabled(true);
    }
    if (players[1].get_status() == true)
    {
        ui->Player2_check_4->setEnabled(true);
        ui->Player2_fold_4->setEnabled(true);
    }
    if (players[2].get_status() == true)
    {
        ui->Player3_check_4->setEnabled(true);
        ui->Player3_fold_4->setEnabled(true);
    }
    if (players[3].get_status() == true)
    {
        ui->Player4_check_4->setEnabled(true);
        ui->Player4_fold_4->setEnabled(true);
    }

    ui->Player5_check_4->setEnabled(false);
    ui->Player5_fold_4->setEnabled(false);
    ui->Player5_raise_4->setEnabled(false);
    ui->Player5_new_bet_4->setEnabled(true);
    ui->Player5_new_bet_4->setMinimum(current_bet + 1);
    ui->label_52->setEnabled(true);
    ui->label_51->setEnabled(false);
    ui->Player4_new_bet_4->setEnabled(false);
}

void Final_Project::on_Player5_new_bet_4_valueChanged(int arg1)
{
    players[4].get_hands()[0]->set_bet(arg1);
    current_bet = arg1;
    ui->Current_bet_4->setText(QString::number(current_bet));
}

void Final_Project::on_Choose_players_cards_4_currentIndexChanged(int index)
{
    if (index == 0)
    {
        ui->Players_cards_4->setText(QString::fromStdString(""));
    }
    if (index == 1)
    {
        ui->Players_cards_4->setText(QString::fromStdString(players[0].print_hand(0)));
    }
    if (index == 2)
    {
        ui->Players_cards_4->setText(QString::fromStdString(players[1].print_hand(0)));
    }
    if (index == 3)
    {
        ui->Players_cards_4->setText(QString::fromStdString(players[2].print_hand(0)));
    }
    if (index == 4)
    {
        ui->Players_cards_4->setText(QString::fromStdString(players[3].print_hand(0)));
    }
    if (index == 5)
    {
        ui->Players_cards_4->setText(QString::fromStdString(players[4].print_hand(0)));
    }
}

bool custom_sort(Card x, Card y)
{
    return (x.get_face_int() < y.get_face_int());
}

int results_texas_hold_em(Hand& player_hand, Hand play)
{
    int result = 0;
    vector<int> count_rank(13, 0);
    vector<int> count_suit(4, 0);
    play.add_card(player_hand.get_card(0));
    play.add_card(player_hand.get_card(1));

    for (int i = 0; i < play.get_hand().size(); i++)
    {
        count_rank[play.get_hand()[i].get_face_int()]++;
        count_suit[play.get_hand()[i].get_suit_int()]++;
    }

    std::sort(play.get_hand().begin(), play.get_hand().end(), custom_sort);

    bool straight = false;
    bool straight_flush = false;
    bool royal_flush = false;

    if (play.get_hand()[6].get_face_int() == 13 && play.get_hand()[5].get_face_int() == 12 && play.get_hand()[4].get_face_int() == 11 && play.get_hand()[3].get_face_int() == 10 && play.get_hand()[0].get_face_int() == 0)
    {
        straight = true;
        if (play.get_hand()[6].get_suit_int() == play.get_hand()[5].get_suit_int() == play.get_hand()[4].get_suit_int() == play.get_hand()[3].get_suit_int() == play.get_hand()[0].get_suit_int())
            royal_flush = true;
    }

    if (!straight)
    {
        for (int i = 0; i < play.get_size() - 4; i++)
        {
            if (play.get_hand()[i].get_face_int() == play.get_hand()[i + 1].get_face_int() - 1 == play.get_hand()[i + 2].get_face_int() - 2 == play.get_hand()[i + 3].get_face_int() - 3 == play.get_hand()[i + 4].get_face_int() - 4)
            {
                straight = true;
                if (play.get_hand()[i].get_suit_int() == play.get_hand()[i + 1].get_suit_int() == play.get_hand()[i + 2].get_suit_int() == play.get_hand()[i + 3].get_suit_int() == play.get_hand()[i + 4].get_suit_int())
                    straight_flush = true;
                i = play.get_size() - 4;
            }
        }
    }

    if (count(count_rank.begin(), count_rank.end(), 2) == 1)
        result = 1; // pair
    if (count(count_rank.begin(), count_rank.end(), 2) >= 2)
        result = 2; // 2 pair
    if (count(count_rank.begin(), count_rank.end(), 3) >= 1)
        result = 3; // 3 of a kind
    if (straight)
        result = 4; // straight
    if ((count(count_suit.begin(), count_suit.end(), 5) == 1) || (count(count_suit.begin(), count_suit.end(), 6) == 1) || (count(count_suit.begin(), count_suit.end(), 7) == 1))
        result = 5; // flush
    if ((count(count_rank.begin(), count_rank.end(), 3) == 1 && (count(count_rank.begin(), count_rank.end(), 4) == 1 || count(count_rank.begin(), count_rank.end(), 2) >= 1)) || count(count_rank.begin(), count_rank.end(), 3) == 2)
        result = 6; // full house
    if (count(count_rank.begin(), count_rank.end(), 4) == 1)
        result = 7; // four of a kind
    if (straight_flush)
        result = 8;
    if (royal_flush)
        result = 9;
    return result;
}

void Final_Project::on_pushButton_9_clicked()
{
    ui->stackedWidget->setCurrentIndex(10);
    if (players.size() >= 2)
    {
        ui->Player1_Result_3->setText(QString::fromStdString(players[0].get_name()));
        ui->Player2_result_3->setText(QString::fromStdString(players[1].get_name()));
    }
    if (players.size() >= 3)
    {
        ui->Player3_result_3->setText(QString::fromStdString(players[2].get_name()));
    }
    if (players.size() >= 4)
    {
        ui->Player4_result_3->setText(QString::fromStdString(players[3].get_name()));
    }
    if (players.size() == 5)
    {
        ui->Player5_result_3->setText(QString::fromStdString(players[4].get_name()));
    }

    for (int i = 0; i < players.size(); i++)
    {
        pot += players[i].get_hands()[0]->get_bet();
        players[i].lost_round(players[i].get_hands()[0]->get_bet());
    }

    int winning_value = 0;
    int winning_index = 0;

    for (int i = 0; i < players.size(); i++)
    {
        if (players[i].get_status() != true)
            continue;

        if (results_texas_hold_em(*(players[i].get_hands()[0]), play) > winning_value)
        {
            winning_value = results_texas_hold_em(*(players[i].get_hands()[0]), play);
            winning_index = i;
        }
    }

    if (players.size() >= 1)
    {
        if (winning_index = 0)
        {
            ui->Player1_Result_4->setText(QString::fromStdString("Won $") + QString::number(pot));
            players[0].won_round(pot);
        }
        else
        {
            ui->Player1_Result_4->setText(QString::fromStdString("Lost $") + QString::number(players[0].get_hands()[0]->get_bet()));
        }
    }
    if (players.size() >= 2)
    {
        if (winning_index = 1)
        {
            ui->Player2_result_4->setText(QString::fromStdString("Won $") + QString::number(pot));
            players[1].won_round(pot);
        }
        else
        {
            ui->Player2_result_4->setText(QString::fromStdString("Lost $") + QString::number(players[1].get_hands()[0]->get_bet()));
        }
    }
    if (players.size() >= 3)
    {
        if (winning_index = 2)
        {
            ui->Player3_result_4->setText(QString::fromStdString("Won $") + QString::number(pot));
            players[2].won_round(pot);
        }
        else
        {
            ui->Player3_result_4->setText(QString::fromStdString("Lost $") + QString::number(players[2].get_hands()[0]->get_bet()));
        }
    }
    if (players.size() >= 4)
    {
        if (winning_index = 3)
        {
            ui->Player4_result_4->setText(QString::fromStdString("Won $") + QString::number(pot));
            players[3].won_round(pot);
        }
        else
        {
            ui->Player4_result_4->setText(QString::fromStdString("Lost $") + QString::number(players[3].get_hands()[0]->get_bet()));
        }
    }
    if (players.size() == 5)
    {
        if (winning_index = 4)
        {
            ui->Player5_result_4->setText(QString::fromStdString("Won $") + QString::number(pot));
            players[4].won_round(pot);
        }
        else
        {
            ui->Player5_result_4->setText(QString::fromStdString("Lost $") + QString::number(players[4].get_hands()[0]->get_bet()));
        }
    }
}

void Final_Project::on_pushButton_20_clicked()
{
    deck = Deck();
    ui->stackedWidget->setCurrentIndex(1);
    for (int i = 0; i < players.size(); i++)
    {
        while (players[i].get_hands().size() > 1)
            players[i].get_hands().pop_back();
        while (players[i].get_hands()[0]->get_size() > 0)
            players[i].get_hands()[0]->remove_last_card();
    }
}

void Final_Project::on_Player1_bet_craps_valueChanged(int arg1)
{
    ui->Player1_roll->setEnabled(true);
}

void Final_Project::on_Player1_roll_clicked()
{
    ui->Player1_bet_craps->setEnabled(false);
    ui->Player1_roll->setEnabled(false);
    players[0].get_hands()[0]->set_bet(ui->Player1_bet_craps->value());
    srand((int)time(0));

    int first_die = rand() % 6 + 1;
    int second_die = rand() % 6 + 1;
    int total = first_die + second_die;

    if (total == 2)
    {
        ui->Player1_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(3 * players[0].get_hands()[0]->get_bet()));
        players[0].won_round(3 * players[0].get_hands()[0]->get_bet());
    }
    else if (total == 3 || total == 4)
    {
        ui->Player1_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(players[0].get_hands()[0]->get_bet()));
        players[0].won_round(players[0].get_hands()[0]->get_bet());
    }
    else if (total == 10 || total == 11)
    {
        ui->Player1_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(2 * players[0].get_hands()[0]->get_bet()));
        players[0].won_round(2 * players[0].get_hands()[0]->get_bet());
    }
    else if (total == 12)
    {
        ui->Player1_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(5 * players[0].get_hands()[0]->get_bet()));
        players[0].won_round(5 * players[0].get_hands()[0]->get_bet());
    }
    else
    {
        ui->Player1_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; lost $") + QString::number(3 * players[0].get_hands()[0]->get_bet()));
        players[0].lost_round(players[0].get_hands()[0]->get_bet());
    }
}

void Final_Project::on_Player2_bet_craps_valueChanged(int arg1)
{
    ui->Player2_roll->setEnabled(true);
}

void Final_Project::on_Player2_roll_clicked()
{
    ui->Player2_bet_craps->setEnabled(false);
    ui->Player2_roll->setEnabled(false);
    players[1].get_hands()[1]->set_bet(ui->Player2_bet_craps->value());
    srand((int)time(0));

    int first_die = rand() % 6 + 1;
    int second_die = rand() % 6 + 1;
    int total = first_die + second_die;

    if (total == 2)
    {
        ui->Player2_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(3 * players[1].get_hands()[0]->get_bet()));
        players[1].won_round(3 * players[1].get_hands()[0]->get_bet());
    }
    else if (total == 3 || total == 4)
    {
        ui->Player2_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(players[1].get_hands()[0]->get_bet()));
        players[1].won_round(players[1].get_hands()[0]->get_bet());
    }
    else if (total == 10 || total == 11)
    {
        ui->Player2_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(2 * players[1].get_hands()[0]->get_bet()));
        players[1].won_round(2 * players[1].get_hands()[0]->get_bet());
    }
    else if (total == 12)
    {
        ui->Player2_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(5 * players[1].get_hands()[0]->get_bet()));
        players[1].won_round(5 * players[1].get_hands()[0]->get_bet());
    }
    else
    {
        ui->Player2_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; lost $") + QString::number(3 * players[1].get_hands()[0]->get_bet()));
        players[1].lost_round(players[1].get_hands()[0]->get_bet());
    }
}

void Final_Project::on_Player3_bet_craps_valueChanged(int arg1)
{
    ui->Player3_roll->setEnabled(true);
}

void Final_Project::on_Player3_roll_clicked()
{
    ui->Player3_bet_craps->setEnabled(false);
    ui->Player3_roll->setEnabled(false);
    players[2].get_hands()[1]->set_bet(ui->Player3_bet_craps->value());
    srand((int)time(0));

    int first_die = rand() % 6 + 1;
    int second_die = rand() % 6 + 1;
    int total = first_die + second_die;

    if (total == 2)
    {
        ui->Player3_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(3 * players[2].get_hands()[0]->get_bet()));
        players[2].won_round(3 * players[2].get_hands()[0]->get_bet());
    }
    else if (total == 3 || total == 4)
    {
        ui->Player3_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(players[2].get_hands()[0]->get_bet()));
        players[2].won_round(players[2].get_hands()[0]->get_bet());
    }
    else if (total == 10 || total == 11)
    {
        ui->Player3_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(2 * players[2].get_hands()[0]->get_bet()));
        players[2].won_round(2 * players[2].get_hands()[0]->get_bet());
    }
    else if (total == 12)
    {
        ui->Player3_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(5 * players[2].get_hands()[0]->get_bet()));
        players[2].won_round(5 * players[2].get_hands()[0]->get_bet());
    }
    else
    {
        ui->Player3_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; lost $") + QString::number(3 * players[2].get_hands()[0]->get_bet()));
        players[2].lost_round(players[2].get_hands()[0]->get_bet());
    }
}

void Final_Project::on_Player4_bet_craps_valueChanged(int arg1)
{
    ui->Player4_roll->setEnabled(true);
}

void Final_Project::on_Player4_roll_clicked()
{
    ui->Player4_bet_craps->setEnabled(false);
    ui->Player4_roll->setEnabled(false);
    players[3].get_hands()[1]->set_bet(ui->Player4_bet_craps->value());
    srand((int)time(0));

    int first_die = rand() % 6 + 1;
    int second_die = rand() % 6 + 1;
    int total = first_die + second_die;

    if (total == 2)
    {
        ui->Player4_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(3 * players[3].get_hands()[0]->get_bet()));
        players[3].won_round(3 * players[3].get_hands()[0]->get_bet());
    }
    else if (total == 3 || total == 4)
    {
        ui->Player4_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(players[3].get_hands()[0]->get_bet()));
        players[3].won_round(players[3].get_hands()[0]->get_bet());
    }
    else if (total == 10 || total == 11)
    {
        ui->Player4_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(2 * players[3].get_hands()[0]->get_bet()));
        players[3].won_round(2 * players[3].get_hands()[0]->get_bet());
    }
    else if (total == 12)
    {
        ui->Player4_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(5 * players[3].get_hands()[0]->get_bet()));
        players[3].won_round(5 * players[3].get_hands()[0]->get_bet());
    }
    else
    {
        ui->Player4_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; lost $") + QString::number(3 * players[3].get_hands()[0]->get_bet()));
        players[3].lost_round(players[3].get_hands()[0]->get_bet());
    }
}

void Final_Project::on_Player5_bet_craps_valueChanged(int arg1)
{
    ui->Player5_roll->setEnabled(true);
}

void Final_Project::on_Player5_roll_clicked()
{
    ui->Player5_bet_craps->setEnabled(false);
    ui->Player5_roll->setEnabled(false);
    players[4].get_hands()[1]->set_bet(ui->Player5_bet_craps->value());
    srand((int)time(0));

    int first_die = rand() % 6 + 1;
    int second_die = rand() % 6 + 1;
    int total = first_die + second_die;

    if (total == 2)
    {
        ui->Player5_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(3 * players[4].get_hands()[0]->get_bet()));
        players[4].won_round(3 * players[4].get_hands()[0]->get_bet());
    }
    else if (total == 3 || total == 4)
    {
        ui->Player5_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(players[4].get_hands()[0]->get_bet()));
        players[4].won_round(players[4].get_hands()[0]->get_bet());
    }
    else if (total == 10 || total == 11)
    {
        ui->Player5_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(2 * players[4].get_hands()[0]->get_bet()));
        players[4].won_round(2 * players[4].get_hands()[0]->get_bet());
    }
    else if (total == 12)
    {
        ui->Player5_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; won $") + QString::number(5 * players[4].get_hands()[0]->get_bet()));
        players[4].won_round(5 * players[4].get_hands()[0]->get_bet());
    }
    else
    {
        ui->Player5_results_craps->setText(QString::fromStdString("Result: Die 1 = ") + QString::number(first_die) + QString::fromStdString(", Die 2 = ") + QString::number(second_die) + QString::fromStdString("; lost $") + QString::number(players[3].get_hands()[4]->get_bet()));
        players[4].lost_round(players[4].get_hands()[0]->get_bet());
    }
}

void Final_Project::on_New_game_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    for (int i = 0; i < players.size(); i++)
    {
        players[i].get_hands()[0]->set_bet(0);
    }
}
