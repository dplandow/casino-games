#include "Cards.h"

Card::Card()
{
    face = 0;
    suit = 0;
}

Card::Card(int suit, int face)
{
    this->face = face;
    this->suit = suit;
}

int Card::get_face_value() const
{
    int face_int = 0;
    switch (face)
    {
        case 0: face_int = 1; break;
        case 1: face_int = 2; break;
        case 2: face_int = 3; break;
        case 3: face_int = 4; break;
        case 4: face_int = 5; break;
        case 5: face_int = 6; break;
        case 6: face_int = 7; break;
        case 7: face_int = 8; break;
        case 8: face_int = 9; break;
        case 9: face_int = 10; break;
        case 10: face_int = 10; break;
        case 11: face_int = 10; break;
        case 12: face_int = 10; break;
        case 13: face_int = 11; break;
        default: break;
    }
    return face_int;
}

int Card::get_suit_int() const
{
    return suit;
}

int Card::get_face_int() const
{
    return face;
}

string Card::get_suit() const
{
    string suit_string;
    switch (suit)
    {
        case 0: suit_string = "H"; break;
        case 1: suit_string = "D"; break;
        case 2: suit_string = "C"; break;
        case 3: suit_string = "S"; break;
        default: break;
    }
    return suit_string;
}

string Card::get_face() const
{
    string face_string;
    switch (face)
    {
        case 0: face_string = "A"; break;
        case 1: face_string = "2"; break;
        case 2: face_string = "3"; break;
        case 3: face_string = "4"; break;
        case 4: face_string = "5"; break;
        case 5: face_string = "6"; break;
        case 6: face_string = "7"; break;
        case 7: face_string = "8"; break;
        case 8: face_string = "9"; break;
        case 9: face_string = "10"; break;
        case 10: face_string = "J"; break;
        case 11: face_string = "Q"; break;
        case 12: face_string = "K"; break;
        case 13: face_string = "A"; break;
        default: break;
    }
    return face_string;
}

string Card::print() const
{
    return (this->get_face() + this->get_suit());
}

void Card::change_face_ace()
{
    face = 13;
}

bool Card::operator<(Card& rhs)
{
    if (this->face < rhs.face)
        return true;
    else
        return false;
}

Deck::Deck()
{
    for (int i = 0; i < 13; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            Card card (j, i);
            deck.push_back(card);
        }
    }
}

void Deck::shuffle()
{
    srand((int)time(0));
    int index;
    for (int i = 0; i < 52; i++)
    {
        index = rand() % 52;
        Card temporary = deck[index];
        deck[index] = deck[i];
        deck[i] = temporary;
    }
}

Card Deck::deal_card()
{
    Card temporary = deck[deck.size() - 1];
    deck.pop_back();
    return temporary;
}

int Deck::size() const
{
    return deck.size();
}

Hand::Hand()
{
    bet = 0;
}

Hand::Hand(int bet)
{
    this->bet = bet;
}

int Hand::get_bet() const
{
    return bet;
}

Card& Hand::get_card(int i)
{
    return hand[i];
}

void Hand::set_bet(int i)
{
    bet = i;
}

void Hand::increase_bet(int value)
{
    bet += value;
}

int Hand::get_sum() const
{
    int sum = 0;
    for (int i = 0; i < hand.size(); i++)
    {
        sum += hand[i].get_face_value();
    }
    return sum;
}

void Hand::add_card(Card new_card)
{
    hand.push_back(new_card);
}

void Hand::remove_last_card()
{
    hand.pop_back();
}

int Hand::get_size()
{
    return hand.size();
}

bool Hand::check_for_aces()
{
    bool ace = false;
    for (int i = 0; i < hand.size(); i++)
    {
        if (hand[i].get_face() == "A")
            ace = true;
    }
    
    return ace;
}

vector<Card>& Hand::get_hand()
{
    return hand;
}

string Hand::print_cards()
{
    string out = "";
    for (int j = 0; j < hand.size(); j++)
    {
        if (j < hand.size() - 1)
            out += hand[j].print() + ", ";
        else
            out += hand[j].print();
    }
    return out;
}

Player::Player()
{
    this->name = "";
    this->balance = 0;
    this->hands.push_back(new Hand(0));
}

Player::Player(string name, int balance)
{
    this->name = name;
    this->balance = balance;
    this->hands.push_back(new Hand(0));
}

string Player::get_name() const
{
    return name;
}

int Player::get_money() const
{
    return balance;
}

void Player::lost_round(int amount)
{
    balance -= amount;
}

void Player::won_round(int amount)
{
    balance += amount;
}

vector<Hand*>& Player::get_hands()
{
    return hands;
}

string Player::print_hand(int i) const
{
    string out = "";
    for (int j = 0; j < hands[i]->get_size(); j++)
    {
        if (j < hands[i]->get_size() - 1)
            out += hands[i]->get_card(j).print() + ", ";
        else
            out += hands[i]->get_card(j).print();
    }
    return out;
}

void Player::add_hand(Hand* hand)
{
    hands.push_back(hand);
}

void Player::change_name(string new_name)
{
    name = new_name;
}

bool Player::get_status() const
{
    return active;
}

void Player::change_status(bool new_status)
{
    active = new_status;
}

Player::~Player()
{
    for (Hand* i : hands)
    {
        i = NULL;
        delete i;
    }
}


