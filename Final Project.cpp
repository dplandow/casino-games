#include "Cards.h"
#include <algorithm>

using namespace std;

vector<Player> create_players ()
{
    int number_players = 0;
    vector<Player> players;
    cout << "Enter number of players: ";
    cin >> number_players;
    
    for (int i = 0; i < number_players; i++)
    {
        string name = "";
        int amount = 0;
        cout << endl;
        
        cout << "Player " << i + 1 << ":" << endl;
        
        cout << "\t" << "Name: ";
        cin >> name;
        
        cout << "\t" << "Amount: $";
        cin >> amount;
        
        players.push_back(Player(name, amount));
    }
    cout << endl;
    
    return players;
}

void hit (Hand& hand, Deck& deck)
{
    hand.add_card(deck.deal_card());
}

void double_down(Hand& hand, Deck& deck)
{
    hand.set_bet(2 * hand.get_bet());
    hit (hand, deck);
}

void split(Player& player, Deck& deck, bool& aces, bool& ace_high)
{
    player.add_hand(new Hand(player.get_hands()[0]->get_bet()));
    player.get_hands()[player.get_hands().size() - 2]->remove_last_card();
    
    player.get_hands()[player.get_hands().size() - 1]->add_card(player.get_hands()[player.get_hands().size() - 2]->get_card(1));
    
    for (int i = player.get_hands().size() - 1; i >= 0; i--)
    {
        player.get_hands()[i]->add_card(deck.deal_card());
        
        player.print_hand(i);
        cout << endl;
        
        if (aces && ace_high && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace")
        {
            player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
        }
        
        if (!aces && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace" && ((player.get_hands()[i]->get_sum() + 10) <= 21))
        {
            aces = true;
            string high_low;
            cout << "ace high or low (h or l): ";
            cin >> high_low;
            
            if (high_low == "h")
            {
                ace_high = true;
                player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
            }
        }
        
        string response;
        
        if (player.get_money() >= (player.get_hands().size() + 1) * player.get_hands()[i]->get_bet() && player.get_hands()[i]->get_card(0).get_face() == player.get_hands()[i]->get_card(1).get_face())
        {
            cout << "hit, stay, double down, or split (h, s, dd, or sp): ";
            cin >> response;
            
            if (response == "sp")
            {
                split(player, deck, aces, ace_high);
            }
            else if (response == "dd")
            {
                double_down(*player.get_hands()[i], deck);
                
                player.print_hand(i);
                
                if (aces && ace_high && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace")
                {
                    player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                }
                
                if (!aces && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace" && ((player.get_hands()[i]->get_sum() + 10) <= 21))
                {
                    aces = true;
                    string high_low;
                    cout << "ace high or low (h or l): ";
                    cin >> high_low;
                    
                    if (high_low == "h")
                    {
                        ace_high = true;
                        player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                    }
                }
                
                cout << endl;
            }
            else
            {
                if (response == "s")
                    cout << endl;
                while (response == "h")
                {
                    hit(*player.get_hands()[i], deck);
                    
                    player.print_hand(i);
                    
                    if (aces && ace_high && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace")
                    {
                        player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                    }
                    
                    if (!aces && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace" && ((player.get_hands()[i]->get_sum() + 10) <= 21))
                    {
                        aces = true;
                        string high_low;
                        cout << "ace high or low (h or l): ";
                        cin >> high_low;
                        
                        if (high_low == "h")
                        {
                            ace_high = true;
                            player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                        }
                    }
                    
                    if (player.get_hands()[i]->get_sum() < 21)
                    {
                        cout << endl;
                        cout << "hit or stay (h or s): ";
                        cin >> response;
                        if (response == "s")
                            cout << endl;
                    }
                    else
                    {
                        response = "s";
                        cout << endl;
                    }
                }
            }
        }
        else if (player.get_money() >= 2 * player.get_hands()[i]->get_bet())
        {
            cout << "hit, stay, or double down (h, s, or dd): ";
            cin >> response;
            
            if (response == "dd")
            {
                double_down(*player.get_hands()[i], deck);
                
                player.print_hand(i);
                
                if (aces && ace_high && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace")
                {
                    player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                }
                
                if (!aces && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace" && ((player.get_hands()[i]->get_sum() + 10) <= 21))
                {
                    aces = true;
                    string high_low;
                    cout << "ace high or low (h or l): ";
                    cin >> high_low;
                    
                    if (high_low == "h")
                    {
                        ace_high = true;
                        player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                    }
                }
                
                cout << endl;
            }
            else
            {
                if (response == "s")
                    cout << endl;
                while (response == "h")
                {
                    hit(*player.get_hands()[i], deck);
                    
                    player.print_hand(i);
                    
                    if (aces && ace_high && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace")
                    {
                        player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                    }
                    
                    if (!aces && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace" && ((player.get_hands()[i]->get_sum() + 10) <= 21))
                    {
                        aces = true;
                        string high_low;
                        cout << "ace high or low (h or l): ";
                        cin >> high_low;
                        
                        if (high_low == "h")
                        {
                            ace_high = true;
                            player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                        }
                    }
                    
                    if (player.get_hands()[i]->get_sum() < 21)
                    {
                        cout << endl;
                        cout << "hit or stay (h or s): ";
                        cin >> response;
                        if (response == "s")
                            cout << endl;
                    }
                    else
                    {
                        response = "s";
                        cout << endl;
                    }
                }
            }
        }
        else
        {
            cout << "hit or stay (h or s): ";
            cin >> response;
            
            if (response == "s")
                cout << endl;
            while (response == "h")
            {
                hit(*player.get_hands()[i], deck);
                
                player.print_hand(i);
                
                if (aces && ace_high && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace")
                {
                    player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                }
                
                if (!aces && player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).get_face() == "Ace" && ((player.get_hands()[i]->get_sum() + 10) <= 21))
                {
                    aces = true;
                    string high_low;
                    cout << "ace high or low (h or l): ";
                    cin >> high_low;
                    
                    if (high_low == "h")
                    {
                        ace_high = true;
                        player.get_hands()[i]->get_card(player.get_hands()[i]->get_size() - 1).change_face_ace();
                    }
                }
                
                if (player.get_hands()[i]->get_sum() < 21)
                {
                    cout << endl;
                    cout << "hit or stay (h or s): ";
                    cin >> response;
                    if (response == "s")
                        cout << endl;
                }
                else
                {
                    response = "s";
                    cout << endl;
                }
            }
        }
    }
}

void player_action_blackjack(vector<Player>& players, Deck& deck)
{
    for (Player& i : players)
    {
        string response;
        bool aces = i.get_hands()[0]->check_for_aces();
        bool ace_high = false;
        
        cout << i.get_name() << ": ";
        
        string high_low;
        if (aces)
        {
            cout << endl << "ace high or low (h or l): ";
            cin >> high_low;
            if (high_low == "h")
            {
                ace_high = true;
                for (int j = 0; j < i.get_hands()[0]->get_size(); j++)
                {
                    if (i.get_hands()[0]->get_card(j).get_face() == "Ace")
                        i.get_hands()[0]->get_card(j).change_face_ace();
                }
            }
        }
        
        if (i.get_money() >= ((i.get_hands().size() + 1) * i.get_hands()[0]->get_bet()) && i.get_hands()[0]->get_card(0).get_face() == i.get_hands()[0]->get_card(1).get_face())
        {
            cout << "hit, stay, double down, or split (h, s, dd, or sp): ";
            cin >> response;
            
            if (response == "sp")
            {
                cout << endl;
                split(i, deck, aces, ace_high);
            }
            else if (response == "dd")
            {
                double_down(*i.get_hands()[0], deck);
                
                i.print_hand(0);
                
                if (aces && ace_high && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace")
                {
                    i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                }
                
                if (!aces && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace" && ((i.get_hands()[0]->get_sum() + 10) <= 21))
                {
                    aces = true;
                    string high_low;
                    cout << "ace high or low (h or l): ";
                    cin >> high_low;
                    
                    if (high_low == "h")
                    {
                        ace_high = true;
                        i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                    }
                }
                
                cout << endl;
            }
            else
            {
                if (response == "s")
                    cout << endl;
                while (response == "h")
                {
                    hit(*i.get_hands()[0], deck);
                    
                    i.print_hand(0);
                    
                    if (aces && ace_high && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace")
                    {
                        i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                    }
                    
                    if (!aces && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace" && ((i.get_hands()[0]->get_sum() + 10) <= 21))
                    {
                        aces = true;
                        string high_low;
                        cout << "ace high or low (h or l): ";
                        cin >> high_low;
                        
                        if (high_low == "h")
                        {
                            ace_high = true;
                            i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                        }
                    }
                    
                    if (i.get_hands()[0]->get_sum() < 21)
                    {
                        cout << endl;
                        cout << "hit or stay (h or s): ";
                        cin >> response;
                        if (response == "s")
                            cout << endl;
                    }
                    else
                    {
                        response = "s";
                        cout << endl;
                    }
                }
            }
        }
        else if (i.get_money() >= 2 * i.get_hands()[0]->get_bet())
        {
            cout << "hit, stay, or double down (h, s, or dd): ";
            cin >> response;
            
            if (response == "dd")
            {
                double_down(*i.get_hands()[0], deck);
                
                i.print_hand(0);
                
                if (aces && ace_high && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace")
                {
                    i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                }
                
                if (!aces && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace" && ((i.get_hands()[0]->get_sum() + 10) <= 21))
                {
                    aces = true;
                    string high_low;
                    cout << "ace high or low (h or l): ";
                    cin >> high_low;
                    
                    if (high_low == "h")
                    {
                        ace_high = true;
                        i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                    }
                }
                
                cout << endl;
            }
            else
            {
                if (response == "s")
                    cout << endl;
                while (response == "h")
                {
                    hit(*i.get_hands()[0], deck);
                    
                    i.print_hand(0);
                    
                    if (aces && ace_high && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace")
                    {
                        i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                    }
                    
                    if (!aces && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace" && ((i.get_hands()[0]->get_sum() + 10) <= 21))
                    {
                        aces = true;
                        string high_low;
                        cout << "ace high or low (h or l): ";
                        cin >> high_low;
                        
                        if (high_low == "h")
                        {
                            ace_high = true;
                            i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                        }
                    }
                    
                    if (i.get_hands()[0]->get_sum() < 21)
                    {
                        cout << endl;
                        cout << "hit or stay (h or s): ";
                        cin >> response;
                        if (response == "s")
                            cout << endl;
                    }
                    else
                    {
                        response = "s";
                        cout << endl;
                    }
                }
            }
        }
        else
        {
            cout << "hit or stay (h or s): ";
            cin >> response;
            
            if (response == "s")
                cout << endl;
            while (response == "h")
            {
                hit(*i.get_hands()[0], deck);
                
                i.print_hand(0);
                
                if (aces && ace_high && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace")
                {
                    i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                }
                
                if (!aces && i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).get_face() == "Ace" && ((i.get_hands()[0]->get_sum() + 10) <= 21))
                {
                    aces = true;
                    string high_low;
                    cout << "ace high or low (h or l): ";
                    cin >> high_low;
                    
                    if (high_low == "h")
                    {
                        ace_high = true;
                        i.get_hands()[0]->get_card(i.get_hands()[0]->get_size() - 1).change_face_ace();
                    }
                }
                
                if (i.get_hands()[0]->get_sum() < 21)
                {
                    cout << endl;
                    cout << "hit or stay (h or s): ";
                    cin >> response;
                    if (response == "s")
                        cout << endl;
                }
                else
                {
                    response = "s";
                    cout << endl;
                }
            }
        }
        
        cout << endl;
    }
}

void blackjack(vector<Player>& players)
{
    srand((int)time(0));
    Deck deck = Deck();
    deck.shuffle();
    
    Player dealer("Dealer", 0);
    
    for (Player& i : players)
    {
        int bet = 0;
        cout << i.get_name() << ", enter bet: $";
        cin >> bet;
        while (bet > i.get_money())
        {
            cout << "Invalid amount. " << i.get_name() << ", enter bet: $";
            cin >> bet;
        }
        i.get_hands()[0]->set_bet(bet);
    }
    
    while (dealer.get_hands()[0]->get_size() < 2)
    {
        for (Player& i : players)
        {
            i.get_hands()[0]->add_card(deck.deal_card());
        }
        dealer.get_hands()[0]->add_card(deck.deal_card());
    }
    
    cout << endl << endl;
    
    cout << dealer.get_name() << "'s card:" << endl;
    cout << "\t" << dealer.get_hands()[0]->get_card(0).print() << endl << endl;
    
    for (const Player& i : players)
    {
        cout << i.get_name() << "'s cards:" << endl;
        i.print_hand(0);
        cout << endl;
    }
    cout << endl;
    
    player_action_blackjack(players, deck);
    
    while (dealer.get_hands()[0]->get_sum() < 17)
    {
        dealer.get_hands()[0]->add_card(deck.deal_card());
    }
    
    cout << dealer.get_name() << "'s cards: " << endl;
    
    dealer.print_hand(0);
    
    cout << endl << endl;
    
    cout << "Results:" << endl;
    for (Player& i : players)
    {
        cout << "\t" << i.get_name() << ": ";
        if (i.get_hands().size() == 1)
        {
            if (i.get_hands()[0]->get_sum() > 21)
            {
                cout << "busted (lose $" << i.get_hands()[0]->get_bet() << ")" << endl;
                i.lost_round(i.get_hands()[0]->get_bet());
            }
            else if (i.get_hands()[0]->get_sum() < dealer.get_hands()[0]->get_sum() && dealer.get_hands()[0]->get_sum() <= 21)
            {
                cout << "lost to dealer (lose $" << i.get_hands()[0]->get_bet() << ")" << endl;
                i.lost_round(i.get_hands()[0]->get_bet());
            }
            else if (i.get_hands()[0]->get_sum() <= 21 && dealer.get_hands()[0]->get_sum() > 21)
            {
                cout << "beat dealer (win $" << i.get_hands()[0]->get_bet() << ")" << endl;
                i.won_round(i.get_hands()[0]->get_bet());
            }
            else if (i.get_hands()[0]->get_sum() == dealer.get_hands()[0]->get_sum())
            {
                cout << "push (no gain/loss)" << endl;
            }
            else
            {
                cout << "beat dealer (win $" << i.get_hands()[0]->get_bet() << ")" << endl;
                i.won_round(i.get_hands()[0]->get_bet());
            }
        }
        else
        {
            cout << endl;
            for (int j = 0; j < i.get_hands().size(); j++)
            {
                cout << "\t\tHand " << j + 1 << ": ";
                if (i.get_hands()[j]->get_sum() > 21)
                {
                    cout << "busted (lose $" << i.get_hands()[j]->get_bet() << ")" << endl;
                    i.lost_round(i.get_hands()[j]->get_bet());
                }
                else if (i.get_hands()[j]->get_sum() < dealer.get_hands()[0]->get_sum() && dealer.get_hands()[0]->get_sum() <= 21)
                {
                    cout << "lost to dealer (lose $" << i.get_hands()[j]->get_bet() << ")" << endl;
                    i.lost_round(i.get_hands()[j]->get_bet());
                }
                else if (i.get_hands()[j]->get_sum() <= 21 && dealer.get_hands()[0]->get_sum() > 21)
                {
                    cout << "beat dealer (win $" << i.get_hands()[j]->get_bet() << ")" << endl;
                    i.won_round(i.get_hands()[j]->get_bet());
                }
                else if (i.get_hands()[j]->get_sum() == dealer.get_hands()[0]->get_sum())
                {
                    cout << "push (no gain/loss)" << endl;
                }
                else
                {
                    cout << "beat dealer (win $" << i.get_hands()[j]->get_bet() << ")" << endl;
                    i.won_round(i.get_hands()[j]->get_bet());
                }
            }
        }
    }
}

int get_max_bet(const vector<Player>& players)
{
    int max_bet = players[0].get_money();
    for (int i = 0; i < players.size(); i++)
    {
        if (players[i].get_money() < max_bet)
            max_bet = players[i].get_money();
    }
    return max_bet;
}

bool custom_sort(Card x, Card y)
{
    return (x.get_face_int() < y.get_face_int());
}

int results_texas_hold_em(Hand& player_hand, Hand play)
{
    int result = 0;
    vector<int> count_rank(13, 0);
    vector<int> count_suit(4, 0);
    play.add_card(player_hand.get_card(0));
    play.add_card(player_hand.get_card(1));
    
    for (int i = 0; i < play.get_hand().size(); i++)
    {
        count_rank[play.get_hand()[i].get_face_int()]++;
        count_suit[play.get_hand()[i].get_suit_int()]++;
    }
    
    sort(play.get_hand().begin(), play.get_hand().end(), custom_sort);
    
    bool straight = false;
    bool straight_flush = false;
    bool royal_flush = false;
    
    if (play.get_hand()[6].get_face_int() == 13 && play.get_hand()[5].get_face_int() == 12 && play.get_hand()[4].get_face_int() == 11 && play.get_hand()[3].get_face_int() == 10 && play.get_hand()[0].get_face_int() == 0)
    {
        straight = true;
        if (play.get_hand()[6].get_suit_int() == play.get_hand()[5].get_suit_int() == play.get_hand()[4].get_suit_int() == play.get_hand()[3].get_suit_int() == play.get_hand()[0].get_suit_int())
            royal_flush = true;
    }
    
    if (!straight)
    {
        for (int i = 0; i < play.get_size() - 4; i++)
        {
            if (play.get_hand()[i].get_face_int() == play.get_hand()[i + 1].get_face_int() - 1 == play.get_hand()[i + 2].get_face_int() - 2 == play.get_hand()[i + 3].get_face_int() - 3 == play.get_hand()[i + 4].get_face_int() - 4)
            {
                straight = true;
                if (play.get_hand()[i].get_suit_int() == play.get_hand()[i + 1].get_suit_int() == play.get_hand()[i + 2].get_suit_int() == play.get_hand()[i + 3].get_suit_int() == play.get_hand()[i + 4].get_suit_int())
                    straight_flush = true;
                i = play.get_size() - 4;
            }
        }
    }
    
    if (count(count_rank.begin(), count_rank.end(), 2) == 1)
        result = 1; // pair
    if (count(count_rank.begin(), count_rank.end(), 2) >= 2)
        result = 2; // 2 pair
    if (count(count_rank.begin(), count_rank.end(), 3) >= 1)
        result = 3; // 3 of a kind
    if (straight)
        result = 4; // straight
    if ((count(count_suit.begin(), count_suit.end(), 5) == 1) || (count(count_suit.begin(), count_suit.end(), 6) == 1) || (count(count_suit.begin(), count_suit.end(), 7) == 1))
        result = 5; // flush
    if ((count(count_rank.begin(), count_rank.end(), 3) == 1 && (count(count_rank.begin(), count_rank.end(), 4) == 1 || count(count_rank.begin(), count_rank.end(), 2) >= 1)) || count(count_rank.begin(), count_rank.end(), 3) == 2)
        result = 6; // full house
    if (count(count_rank.begin(), count_rank.end(), 4) == 1)
        result = 7; // four of a kind
    if (straight_flush)
        result = 8;
    if (royal_flush)
        result = 9;
    return result;
}

void player_action_texas_hold_em(vector<Player>& players, const int& maximum_bet, int& current_bet)
{
    bool bet_raised = false;
    
    for (int i = 0; i < players.size(); i++)
    {
        if (players[i].get_status() != true)
            continue;
        cout << "Current bet: $" << current_bet << ", Maximum bet: $" << maximum_bet << "." << endl;
        string response;
        if (current_bet < maximum_bet)
            cout << players[i].get_name() << ", check, raise, or fold (c, r, or f): ";
        else
            cout << players[i].get_name() << ", check or fold (c or f): ";
        cin >> response;
        if (response == "c")
        {
            cout << "\tDeducted $" << current_bet - players[i].get_hands()[0]->get_bet() << "." << endl << endl;
            players[i].get_hands()[0]->set_bet(current_bet);
        }
        else if (response == "r")
        {
            bet_raised = true;
            int new_bet = 0;
            
            cout << "\tEnter bet: $";
            cin >> new_bet;
            while (new_bet > maximum_bet || new_bet < current_bet)
            {
                if (new_bet > maximum_bet)
                {
                    cout << "\tInvalid entry (greater than maximum bet). Enter bet: $";
                    cin >> new_bet;
                }
                else
                {
                    cout << "\tInvalid entry (less than current bet). Enter bet: $";
                    cin >> new_bet;
                }
            }
            cout << "\tDeduced $" << new_bet - players[i].get_hands()[0]->get_bet() << "." << endl << endl;
            players[i].get_hands()[0]->set_bet(new_bet);
            
            current_bet = new_bet;
        }
        else
        {
            players[i].change_status(false);
            cout << endl;
        }
    }
    
    if (bet_raised)
    {
        for (int i = 0; i < players.size(); i++)
        {
            if (players[i].get_status() != true)
                continue;
            cout << "Current bet: $" << current_bet << "." << endl;
            
            if (players[i].get_hands()[0]->get_bet() < current_bet)
            {
                string response;
                cout << players[i].get_name() << ", check or fold (c or f): ";
                cin >> response;
                if (response == "c")
                {
                    cout << "\tDeducted $" << current_bet - players[i].get_hands()[0]->get_bet() << "." << endl << endl;
                    players[i].get_hands()[0]->set_bet(current_bet);
                }
                else
                {
                    players[i].change_status(false);
                    cout << endl;
                }
            }
        }
    }
}


void texas_hold_em(vector<Player>& players)
{
    int minimum_bet = 0;
    int small_blind_index = 0;
    string response = "";
    
    do
    {
        for (int i = 0; i < players.size(); i++)
        {
            players[i].change_status(true);
        }
        int maximum_bet = get_max_bet(players);
        cout << "Maximum bet: $" << maximum_bet << "." << endl;
        cout << "Enter big blind (must be less than maximum bet): $";
        cin >> minimum_bet;
        while (minimum_bet > maximum_bet)
        {
            cout << "Invalid entry (greater than maximum bet). Enter big blind (must be less than maximum bet): $";
            cin >> minimum_bet;
        }
        cout << "Minimum bet: $" << minimum_bet << endl;
        
        int big_blind = minimum_bet;
        int small_blind = minimum_bet / 2;
        
        rotate(players.begin(), players.begin() + small_blind_index, players.end());
        
        int current_bet = minimum_bet;
        int pot = 0;
        
        srand((int)time(0));
        Deck deck = Deck();
        deck.shuffle();
        
        players[0].get_hands()[0]->set_bet(small_blind);
        players[1].get_hands()[0]->set_bet(big_blind);
        
        cout << "Deducted small blind ($" << small_blind << ") from " << players[0].get_name() << "'s money and big blind ($" << big_blind << ") from " << players[1].get_name() << "'s money." << endl << endl;
        
        while (players[0].get_hands()[0]->get_size() < 2)
        {
            for (int i = 0; i < players.size(); i++)
            {
                players[i].get_hands()[0]->add_card(deck.deal_card());
            }
        }
        
        for (int i = 0; i < players.size(); i++)
        {
            cout << players[i].get_name() << "'s cards:" << endl;
            cout << "\t" << players[i].get_hands()[0]->get_card(0).get_face() << " of " << players[i].get_hands()[0]->get_card(0).get_suit() << endl;
            cout << "\t" << players[i].get_hands()[0]->get_card(1).get_face() << " of " << players[i].get_hands()[0]->get_card(1).get_suit() << endl << endl;
        }
        
        rotate(players.begin(), players.begin() + 2, players.end());
        
        player_action_texas_hold_em(players, maximum_bet, current_bet);
        
        Hand discard;
        Hand play;
        
        discard.add_card(deck.deal_card());
        
        play.add_card(deck.deal_card());
        play.add_card(deck.deal_card());
        play.add_card(deck.deal_card());
        
        cout << "Flop:" << endl;
        
        for (int i = 0; i < play.get_size(); i++)
        {
            cout << "\t" << play.get_card(i).get_face() << " of " << play.get_card(i).get_suit() << endl;
        }
        
        cout << endl;
        
        player_action_texas_hold_em(players, maximum_bet, current_bet);
        
        discard.add_card(deck.deal_card());
        
        play.add_card(deck.deal_card());
        
        cout << "Turn:" << endl;
        
        for (int i = 0; i < play.get_size(); i++)
        {
            cout << "\t" << play.get_card(i).get_face() << " of " << play.get_card(i).get_suit() << endl;
        }
        
        cout << endl;
        
        player_action_texas_hold_em(players, maximum_bet, current_bet);
        
        discard.add_card(deck.deal_card());
        
        play.add_card(deck.deal_card());
        
        cout << "River:" << endl;
        
        for (int i = 0; i < play.get_size(); i++)
        {
            cout << "\t" << play.get_card(i).get_face() << " of " << play.get_card(i).get_suit() << endl;
        }
        
        cout << endl;
        
        player_action_texas_hold_em(players, maximum_bet, current_bet);
        
        for (int i = 0; i < players.size(); i++)
        {
            pot += players[i].get_hands()[0]->get_bet();
            players[i].lost_round(players[i].get_hands()[0]->get_bet());
        }
        
        int winning_value = 0;
        int winning_index = 0;
        
        for (int i = 0; i < players.size(); i++)
        {
            if (players[i].get_status() != true)
                continue;
            
            if (results_texas_hold_em(*(players[i].get_hands()[0]), play) > winning_value)
            {
                winning_value = results_texas_hold_em(*(players[i].get_hands()[0]), play);
                winning_index = i;
            }
        }
        
        cout << "Results: " << players[winning_index].get_name() << " wins $" << pot << "." << endl << endl;
        players[winning_index].won_round(pot);
        small_blind_index++;
        
        cout << "Would you like to play another round? (y or n): ";
        cin >> response;
    }
    while (response == "y");
}

void craps(vector<Player>& players)
{
    int first_die = 0;
    int second_die = 0;
    int bet = 0;
    int total = 0;
    string response = "";
    
    srand((int)time(0));
    
    for (int i = 0; i < players.size(); i++)
    {
        cout << players[i].get_name() << ":" << endl;
        do
        {
            cout << "\tEnter bet: $";
            cin >> bet;
            while (bet > players[i].get_money())
            {
                cout << "\tInvalid amount. Enter bet: $";
                cin >> bet;
            }
            
            srand((int)time(0));
            players[i].get_hands()[0]->set_bet(bet);
            first_die = rand() % 6 + 1;
            second_die = rand() % 6 + 1;
            total = first_die + second_die;
            
            cout << "\tDie 1: " << first_die << endl;
            cout << "\tDie 2: " << second_die << endl;
            cout << "\tTotal: " << total << endl;
            
            if (total == 2)
            {
                cout << "\tResult: won $" << 3 * players[i].get_hands()[0]->get_bet() << endl << endl;
                players[i].won_round(3 * players[i].get_hands()[0]->get_bet());
            }
            else if (total == 3 || total == 4)
            {
                cout << "\tResult: won $" << players[i].get_hands()[0]->get_bet() << endl << endl;
                players[i].won_round(players[i].get_hands()[0]->get_bet());
            }
            else if (total == 10 || total == 11)
            {
                cout << "\tResult: won $" << 2 * players[i].get_hands()[0]->get_bet() << endl << endl;
                players[i].won_round(2 * players[i].get_hands()[0]->get_bet());
            }
            else if (total == 12)
            {
                cout << "\tResult: won $" << 5 * players[i].get_hands()[0]->get_bet() << endl << endl;
                players[i].won_round(5 * players[i].get_hands()[0]->get_bet());
            }
            else
            {
                cout << "\tResult: lost $" << players[i].get_hands()[0]->get_bet() << endl << endl;
                players[i].lost_round(players[i].get_hands()[0]->get_bet());
            }
            
            if (players[i].get_money() > 0)
            {
                cout << "\tWould you like to play another round? (y or n): ";
                cin >> response;
            }
            cout << endl;
        }
        while (players[i].get_money() > 0 && response == "y");
    }
}

int main()
{
    vector<Player> players = create_players();
    blackjack(players);
}
