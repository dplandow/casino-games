#ifndef FINAL_PROJECT_H
#define FINAL_PROJECT_H
#include "Cards.h"

#include <QMainWindow>

namespace Ui {
class Final_Project;
}

class Final_Project : public QMainWindow
{
    Q_OBJECT

public:
    explicit Final_Project(QWidget *parent = 0);
    ~Final_Project();

private slots:

    void on_Number_of_players_valueChanged(int arg1);

    void on_Name_entry_continue_clicked();

    void on_pushButton_4_clicked();

    void on_Blackjack_button_clicked();

    void on_Texas_hold_em_button_clicked();

    void on_Craps_button_clicked();

    void on_Bet_entry_continue_clicked();

    void on_Stay1_clicked();

    void on_Hit1_clicked();

    void on_Double_down1_clicked();

    void on_Split1_clicked();

    void on_Stay1_2_clicked();

    void on_Hit1_2_clicked();

    void on_Double_down1_2_clicked();

    void on_Stay2_clicked();

    void on_Hit2_clicked();

    void on_Double_down2_clicked();

    void on_Split2_clicked();

    void on_Stay2_2_clicked();

    void on_Hit2_2_clicked();

    void on_Double_down2_2_clicked();

    void on_Stay3_clicked();

    void on_Hit3_clicked();

    void on_Double_down3_clicked();

    void on_Split3_clicked();

    void on_Stay3_2_clicked();

    void on_Hit3_2_clicked();

    void on_Double_down3_2_clicked();

    void on_Stay4_clicked();

    void on_Hit4_clicked();

    void on_Double_down4_clicked();

    void on_Split4_clicked();

    void on_Stay4_2_clicked();

    void on_Hit4_2_clicked();

    void on_Double_down4_2_clicked();

    void on_Stay5_1_clicked();

    void on_Hit5_clicked();

    void on_Double_down5_clicked();

    void on_Split5_clicked();

    void on_Stay5_2_clicked();

    void on_Hit5_2_clicked();

    void on_Double_down5_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_Big_blind_valueChanged(int arg1);

    void on_pushButton_5_clicked();

    void on_Player1_check_clicked();

    void on_Player1_fold_clicked();

    void on_Player1_raise_clicked();

    void on_Player2_check_clicked();

    void on_Player2_fold_clicked();

    void on_Player2_raise_clicked();

    void on_Player3_check_clicked();

    void on_Player3_fold_clicked();

    void on_Player3_raise_clicked();

    void on_Player1_new_bet_valueChanged(int arg1);

    void on_Player2_new_bet_valueChanged(int arg1);

    void on_Player3_new_bet_valueChanged(int arg1);

    void on_Player4_check_clicked();

    void on_Player4_fold_clicked();

    void on_Player4_raise_clicked();

    void on_Player4_new_bet_valueChanged(int arg1);

    void on_Player5_check_clicked();

    void on_Player5_fold_clicked();

    void on_Player5_raise_clicked();

    void on_Player5_new_bet_valueChanged(int arg1);

    void on_Choose_players_cards_currentIndexChanged(int index);

    void on_pushButton_6_clicked();

    void on_Player1_check_2_clicked();

    void on_Player1_fold_2_clicked();

    void on_Player1_raise_2_clicked();

    void on_Player1_new_bet_2_valueChanged(int arg1);

    void on_Player2_check_2_clicked();

    void on_Player2_fold_2_clicked();

    void on_Player2_raise_2_clicked();

    void on_Player2_new_bet_2_valueChanged(int arg1);

    void on_Player3_check_2_clicked();

    void on_Player3_fold_2_clicked();

    void on_Player3_raise_2_clicked();

    void on_Player3_new_bet_2_valueChanged(int arg1);

    void on_Player4_check_2_clicked();

    void on_Player4_fold_2_clicked();

    void on_Player4_raise_2_clicked();

    void on_Player4_new_bet_2_valueChanged(int arg1);

    void on_Player5_check_2_clicked();

    void on_Player5_fold_2_clicked();

    void on_Player5_raise_2_clicked();

    void on_Player5_new_bet_2_valueChanged(int arg1);

    void on_Choose_players_cards_2_currentIndexChanged(int index);

    void on_pushButton_7_clicked();

    void on_Player1_check_3_clicked();

    void on_Player1_fold_3_clicked();

    void on_Player1_raise_3_clicked();

    void on_Player1_new_bet_3_valueChanged(int arg1);

    void on_Player2_check_3_clicked();

    void on_Player2_fold_3_clicked();

    void on_Player2_raise_3_clicked();

    void on_Player2_new_bet_3_valueChanged(int arg1);

    void on_Player3_check_3_clicked();

    void on_Player3_fold_3_clicked();

    void on_Player3_raise_3_clicked();

    void on_Player3_new_bet_3_valueChanged(int arg1);

    void on_Player4_check_3_clicked();

    void on_Player4_fold_3_clicked();

    void on_Player4_raise_3_clicked();

    void on_Player4_new_bet_3_valueChanged(int arg1);

    void on_Player5_check_3_clicked();

    void on_Player5_fold_3_clicked();

    void on_Player5_raise_3_clicked();

    void on_Player5_new_bet_3_valueChanged(int arg1);

    void on_Choose_players_cards_3_currentIndexChanged(int index);

    void on_pushButton_8_clicked();

    void on_Player1_check_4_clicked();

    void on_Player1_fold_4_clicked();

    void on_Player1_raise_4_clicked();

    void on_Player1_new_bet_4_valueChanged(int arg1);

    void on_Player2_check_4_clicked();

    void on_Player2_fold_4_clicked();

    void on_Player2_raise_4_clicked();

    void on_Player2_new_bet_4_valueChanged(int arg1);

    void on_Player3_check_4_clicked();

    void on_Player3_fold_4_clicked();

    void on_Player3_raise_4_clicked();

    void on_Player3_new_bet_4_valueChanged(int arg1);

    void on_Player4_check_4_clicked();

    void on_Player4_fold_4_clicked();

    void on_Player4_raise_4_clicked();

    void on_Player4_new_bet_4_valueChanged(int arg1);

    void on_Player5_check_4_clicked();

    void on_Player5_fold_4_clicked();

    void on_Player5_raise_4_clicked();

    void on_Player5_new_bet_4_valueChanged(int arg1);

    void on_Choose_players_cards_4_currentIndexChanged(int index);

    void on_pushButton_9_clicked();

    void on_pushButton_20_clicked();

    void on_Player1_bet_craps_valueChanged(int arg1);

    void on_Player1_roll_clicked();

    void on_Player2_bet_craps_valueChanged(int arg1);

    void on_Player2_roll_clicked();

    void on_Player3_bet_craps_valueChanged(int arg1);

    void on_Player3_roll_clicked();

    void on_Player4_bet_craps_valueChanged(int arg1);

    void on_Player4_roll_clicked();

    void on_Player5_bet_craps_valueChanged(int arg1);

    void on_Player5_roll_clicked();

    void on_New_game_clicked();

private:
    Ui::Final_Project *ui;
    vector<Player> players;
    Deck deck = Deck();
    Player dealer = Player("Dealer", 0);
    int max_bet = 0;
    int current_bet = 0;
    int pot = 0;
    Hand discard;
    Hand play;

};

#endif // FINAL_PROJECT_H
