#ifndef CARDS_H
#define CARDS_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Card
{
public:
    Card();
    Card(int suit, int face);
    int get_face_value() const;
    int get_suit_int() const;
    int get_face_int() const;
    string get_face() const;
    string get_suit() const;
    string print() const;
    void change_face_ace();
    bool operator<(Card& rhs);
    
private:
    int suit;
    int face;
};

class Deck
{
public:
    Deck();
    void shuffle();
    Card deal_card();
    int size() const;
    
private:
    vector<Card> deck;
};

class Hand
{
public:
    Hand();
    Hand(int bet);
    int get_bet() const;
    Card& get_card(int i);
    void set_bet(int i);
    void increase_bet(int value);
    int get_sum() const;
    void add_card(Card new_card);
    void remove_last_card();
    int get_size();
    vector<Card>& get_hand();
    bool check_for_aces();
    string print_cards();
    
private:
    int bet;
    vector<Card> hand;
};

class Player
{
public:
    Player();
    Player(string name, int balance);
    string get_name() const;
    int get_money() const;
    void lost_round(int amount);
    void won_round(int amount);
    vector<Hand*>& get_hands();
    string print_hand(int i) const;
    void add_hand(Hand* hand);
    void change_name(string new_name);
    bool get_status() const;
    void change_status(bool new_status);
    virtual ~Player();
    
private:
    string name;
    int balance;
    vector<Hand*> hands;
    bool active = true;
};

#endif

