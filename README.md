Project: Casino Games (Blackjack, Texas Holdem, Craps)

Goal: Working program that enables the user to choose between playing blackjack, texas holdem, or craps. Initially I will create functions that simulate playing each game, using console input and output. Once this works, I will incorporate Qt, enabling the user to view which cards they have and interact with the games using a mouse/keyboard. The program will keep track of each players money and bets.

Initially, the files "Final Project.cpp", "Cards.h", and "Cards.cpp" when ran enabled the user to play blackjack, texas hold em, or craps using console input and output. Once all three of these games were functioning properly through the console, Qt was incorporated.

Through extensive use of a QStackedWidget, QLabels, QPlainTextEdits, QSpinBoxes, and QPushButton, users are able to play blackjack, texas hold em, or craps using a keyboard/mouse with the Qt application. As few as 1 and up to 5 players are allowed, however texas hold em requires atleast 2. The user can play one round of blackjack, one hand of texas hold em, and one roll of craps per run of the program.

When Qt functionality was added, some of the class definitions had to be modified in order to work with the desired Qt layout. Because of this, when the console input/output version of the program is run, the input/output is not accurate. This was expected and left unchanged, as the Qt application is the primary focus of the project.

Future use of this program could inlcude analysis of the best method for playing each game is. For example, with blackjack one could incorporate a "smart" player that only hits up to 18, and a "dumb" player that hits no matter what. By running the program several times and analyzing which player wins most, it could be determined which approach to the game is more probable to win.